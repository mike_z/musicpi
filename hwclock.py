#!/usr/bin/python
# -*- coding: utf-8 -*-

from hw import ds1302_lib
import sys
from datetime import datetime
import os

DATETIME_FORMAT = "%Y%m%d%H%M%S"
DATETIME_FORMAT2 = "%Y-%m-%d %H:%M:%S"

def main():
    rtc_ds1302 = ds1302_lib.DS1302(pin_io=33, pin_sclk=32, pin_ce=36)
    
    # Jeśli zegar jest zatrzymany uruchomienie go
    if rtc_ds1302.is_clock_stoped():
        rtc_ds1302.clock_halt(False)
    
    # set time
    if len(sys.argv) == 2: # 1 argument to nazwa pliku skryptu
        rtc_ds1302.set_datetime(datetime.strptime(sys.argv[1], DATETIME_FORMAT))
    
    # get time    
    else:
        rtc_time = rtc_ds1302.get_datetime()
        os.system('date  -s "%s"' % rtc_time.strftime(DATETIME_FORMAT2))
        
        print(rtc_time.strftime(DATETIME_FORMAT2))

if __name__ == '__main__':
    main()