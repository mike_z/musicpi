#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on 2 maj 2016

@author: Michał
'''

class MPDUtils:

    '''
    @staticmethod
    def humanize_time(secs):
        secs = int(secs.partition(".")[0])
        mins, secs = divmod(secs, 60)
        hours, mins = divmod(mins, 60)
        secs = str(secs).zfill(2)
        mins = str(mins).zfill(2)
        hours = str(hours).zfill(2)
        return '%sh%sm%ss' % (hours, mins, secs)
    '''
  
    @staticmethod
    def format(song):
        """Formatte joliment un fichier"""
        artiste = song["artist"] if "artist" in song.keys() else "<unkown>"
        titre = song["title"] if "title" in song.keys() else "<unkown>"
      
        if artiste == "<unkown>" and titre == "<unkown>":
            return "%s. %s" % (song["pos"], song["file"])
        else:
            return "%s. %s - %s" % (song["pos"], artiste, titre)

    @staticmethod
    def timempd_to_string(actual_and_pos_time):
                
        # 123:456
        time_s = actual_and_pos_time.split(":")
        result = ""
        
        try:
            c_time = int(time_s[0])
        except (Exception):
            c_time = 0
            
        try:
            t_time = int(time_s[1])
        except (Exception):
            t_time = 0
                                
        cur_h = (c_time / 3600) 
        cur_m = (c_time / 60) % 60
        cur_s = c_time % 60
    
        if cur_h != 0:
            result += ("{0}:").format(cur_h)
            
        result += ("{0:02d}:{1:02d}/").format(cur_m, cur_s)
                        
        tot_h = (t_time / 3600) 
        tot_m = (t_time / 60) % 60
        tot_s = t_time % 60
    
        if tot_h != 0:
            result += ("{0}:").format(tot_h)
        result += ("{0:02d}:{1:02d}").format(tot_m, tot_s)
        
        return result