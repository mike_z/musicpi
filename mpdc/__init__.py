#!/usr/bin/python
# -*- coding: utf-8 -*-

from mpd import MPDClient as mc, MPDError, CommandError , ConnectionError
import time 
import datetime 
from time import sleep
from mpdc.mpd_utils import MPDUtils
from logger import log
#import urllib


#from mpdc import mpdc
    

# -- Do przeniesienia w osobny moduł

class PollerError(Exception):
    """Fatal error in poller."""
    

class MpdClient:
    _instance = None
    
    def __init__(self, host="localhost", port="6600", password=None):
        '''
        Konstruktor z ustawieniami połaczenia dla mpd client
        
        Args:
            host: host z uruchomonym mpd, domyślnie localhost
            port: j.w. port
            password: opcjonalne hasło które można ustawić przy łaczeniu poza localhost
        '''
        self._host = host
        self._port = port
        self._password = password
        self._client = mc()
        
    @staticmethod
    def Instance():
        '''
        Zwraca zawsze jeden obiekt MpdClient przestawiajacy całą obsługę 
        klienta MPD
        '''
        if MpdClient._instance == None:
            MpdClient._instance = MpdClient()
            MpdClient._instance.connect()
            
        try:
            MpdClient._instance._client.stats()
        except ConnectionError:
            try:
                MpdClient._instance.close()
            except:
                pass
            
            MpdClient._instance.connect()
            
        return MpdClient._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)
    
    def __del__(self):
        #self._th_valid = False
        #sleep(self._th_time)
        pass

    # ----------------------------------------------------------------------------------
    # connetion
    
    def connect(self):
        '''
        Łączenie z serwerem MPD
        '''
        try:
            self._client.connect(self._host, self._port)
            
        except IOError as err:
            raise PollerError("Could not connect to '%s': %s" %
                              (self._host, err))
            
        except MPDError as e:
            raise PollerError("Could not connect to '%s': %s" %
                              (self._host, e))
        
        if self._password:
            
            #próba połączenia z hasłem
            try:
                self._client.password(self._password)

            # Problemy z wysłanym hasłem (np. złe hasło)
            except CommandError as e:
                raise PollerError("Could not connect to '%s': "
                                  "password commmand failed: %s" %
                                  (self._host, e))

            # Pozostałe
            except (MPDError, IOError) as e:
                raise PollerError("Could not connect to '%s': "
                                  "error with password command: %s" %
                                  (self._host, e))
                
                
        

    def disconnect(self):
        '''
        Rozłączanie z serwerm MPD
        '''
        try:
            self._client.close()

        # Wymuszenie rozłączenia jeśli normalne zakończenie nie dało rezultatu
        except (MPDError, IOError):
            pass

        try:
            self._client.disconnect()

        # Problem z rozłaczeniem, wymuszenie nowego obiektu klienta
        except (MPDError, IOError):
            self._client = mc()
            
    # ----------------------------------------------------------------------------------
    
    def get_current_song(self):
        
        try:
            csong = self._client.currentsong()
        except (MPDError, IOError):
            csong = []
        return csong
 

    def get_status(self):
        
        try:
            stat = self._client.status()
        except (MPDError, IOError):
            stat = []
        return stat
    
    def get_current_playlist(self):
        return self._client.playlist() 
    
    def get_current_playlistinfo(self):
        return self._client.playlistinfo() 

    def get_devices(self):
        '''
        Pobiera listę urządzeń w katalogu głównym
        '''
        dev_cat = self._client.lsinfo("/")
        dev_cat = sorted(dev_cat, key=lambda tup: tup["directory"])
        
        for entry in dev_cat:
            print("%s" % entry)
        
        return dev_cat
          
    def get_timestring(self):
        return MPDUtils.timempd_to_string(self.get_status().get('time',''))
    
    # ---------------------------------------------------------
    def playback_play(self):
        self._client.play()
    
    def playback_next(self):
        self._client.next()
        
    def playback_previous(self):
        self._client.previous()
    
    def playback_stop(self):
        self._client.stop()
        
    def playback_pause(self, pause=None):
        if pause is None:
            self._client.pause()
        else:
            self._client.pause(pause)
    
    # ---------------------------------------------------------
    
    def count_playlists(self):
        return len(self._client.listplaylists())
    
    def get_playlists(self):
        return self._client.listplaylists()
    
    def use_playlist(self,idListy):
        
        lista = self._client.listplaylists()[idListy]['playlist']
        self._client.clear()
        try:
            log.warning("próba użycia listy " + lista)
            self._client.load(lista)
            self.playback_play()
        except:
            log.fatal("nie ma takiej listy " + lista)
            pass
        


    def get_volume(self):
        return self._client.status().get('volume',0)

    
    def set_volume(self, vol=50):
        
        if vol<0:
            vol = 0
        elif vol>100:
            vol = 100
        try:    
            self._client.setvol(vol)
        except:
            pass
    
    
    def set_volume_rel(self, nvol=None, cvol=None): 
        
        if nvol is None:
            return
        #if cvol is None:
        #    cvol = int(self.get_volume())
                        
        if cvol<0:
            cvol = 0
        elif cvol>100:
            cvol = 100
                
                
                
        log.warning('set_volume_rel ' + str(nvol) + " " + str(cvol))
        
        #wsp = 2
        
        #nvol *= wsp
        nvol += cvol
        
        
        if nvol<0:
            nvol = 0
        elif nvol>100:
            nvol = 100
            
        self.set_volume(nvol)
        
        return nvol
        
        

    def update_database(self, mlib_dir=None):
        '''
        Aktualizuje biliotekę utroworów
        '''
        self._client.update(mlib_dir)
        
    def update_usb_lists(self):
        
        self.musicDirs = ['usb0', 'usb1', 'usb2', 'usb3']
        
        self.update_database()
        
        for mdir in self.musicDirs:     
                  
            try:
                self._client.rm(mdir) # usuwa starą
            except CommandError: # nie ma takiej listy ok
                pass
            
            self._client.clear()    # czyści playlistę główną
            
            try:
                if len(self._client.lsinfo(mdir)) > 0:
                    self._client.add(mdir)
                    self._client.save(mdir)
                    print mdir 
            except CommandError: # nie ma takiej lokalizacji
                pass
            
        pass
    