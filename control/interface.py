#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from music_pi.mp_events import MP_Events
from music_pi.event_map import Event_Map

from control.display import Display
from control.layer_record import Layer_Record

from logger import log

#from logger import log

class Hw_Interface:
    KEY_STATE_DOWN  = 1
    KEY_STATE_UP    = 2
    
    KEY_PREV = 0
    KEY_NEXT = 1
    KEY_MENU = 2
    KEY_BACK = 3
    KEY_PLAY = 7
    KEY_PFOL = 6
    KEY_NFOL = 5
    KEY_PRW  = 4
    
    ALIGN_LEFT      = 0
    ALIGN_CENTER    = 1
    ALIGN_RIGHT     = 2
    SCROLL_TEXT     = 3
    
    # dodatkowe znaki generatora
    SYMBOL_ARROW_RIGHT = 0x7E
    SYMBOL_ARROW_LEFT = 0x7D
    SYMBOL_FULL_BOX = 0xFF
    SYMBOL_LF = 0x0A
    
    
    @staticmethod
    def pwm_set(lcd0=0, lcd1=0, key=0, enc=0):
        '''
        Ustawia PWM na wskazane wartości z zakresu 0-25
        '''
        if lcd0 < 0 or lcd0 > 254:
            lcd0 = 0
        else: 
            lcd0 *= 10
        
        if lcd1 < 0 or lcd1 > 254:
            lcd1 = 0
        else: 
            lcd1 *= 10
            
        if key < 0 or key > 254:
            key = 0
        else: 
            key *= 10
            
        if enc < 0 or enc > 254:
            enc = 0
        else: 
            enc *= 10  
            
        
        ev = MP_Events.Instance()
        ev.new_event(None, Event_Map.EV_PWM_SET, [lcd0, lcd1, key, enc])
     
    @staticmethod   
    def lcd_print(string_text, style=Display.ALIGN_LEFT, start_row=0, row_len=0):
        '''
        Umieszcza tekst na ekranie (w buforze warstw)
        
        Args:
            string_text: ciąg do wyświetlenia, jeśli dłuższy niż wiersz 
                podlega metodą zależnym od stylu
            style: styl formatowania lub przewijania, tj. display.ALIGN_LEFT, display.ALIGN_CENTER,
                display.ALIGN_RIGHT, display.SCROLL_TEXT
            start_row: numer wiersza początkowego
            row_len: (0 = ten sam wiersz) ile wierszy
        '''
        
        new_layer = Layer_Record(string_text, style, start_row, row_len)
        ev = MP_Events.Instance()
        ev.new_event(None, Event_Map.EV_LCD_PRINT, new_layer)
        return new_layer
    
    @staticmethod   
    def lcd_get_layer(string_text, style=Display.ALIGN_LEFT, start_row=0, row_len=0):
        '''
        Generuje warstwę bufora warstw
        
        Args:
            string_text: ciąg do wyświetlenia, jeśli dłuższy niż wiersz 
                podlega metodą zależnym od stylu
            style: styl formatowania lub przewijania, tj. display.ALIGN_LEFT, display.ALIGN_CENTER,
                display.ALIGN_RIGHT, display.SCROLL_TEXT
            start_row: numer wiersza początkowego
            row_len: (0 = ten sam wiersz) ile wierszy
        '''
        
        new_layer = Layer_Record(string_text, style, start_row, row_len)
        return new_layer
    
    @staticmethod     
    def lcd_print_layer(layer):
        '''
        Umieszcza tekst na ekranie (w buforze warstw)
        
        Args:
            layer: wygenerowana wcześniej warstwa (ponowne użycie)
        '''
        
        ev = MP_Events.Instance()
        ev.new_event(None, Event_Map.EV_LCD_PRINT, layer)
    
    @staticmethod
    def lcd_rescroll():
        '''
        Restartuje pozycje przewijania ekranu
        '''
        ev = MP_Events.Instance()
        ev.new_event(None, Event_Map.EV_LCD_SCROLL_RST, None)
        
    @staticmethod
    def lcd_clear():
        '''
        Czyści software-owo zawartość ekranu
        '''
        ev = MP_Events.Instance()
        ev.new_event(None, Event_Map.EV_LCD_CLEAR, None)
        
        
    @staticmethod
    def key_map(key, fun, state=1):
        '''
        Mapuje klawisz do przypisanej metody
        '''
        ev = MP_Events.Instance()
        data = {'key':key, 'fun':fun, 'state':state}
        ev.new_event(None, Event_Map.EV_KEY_MAP, data)

    @staticmethod
    def key_clear_map():
        '''
        Czyści mape przypisaną do klawiszy
        '''
        ev = MP_Events.Instance()
        ev.new_event(None, Event_Map.EV_KEY_MCLEAR, None)
        
                
    
    # Wrapery przez sterowanie sprzętowe przycisków
    
    MP_WAKEUP   = 2 # stand-by to on
    MP_GOSLEEP  = 4 # on to stand-by

    MP_GOON     = 15 # menu to on
    MP_GOMENU   = 31 # on to menu
    
    MP_SHUTDOWN = 1
    
    '''
    @staticmethod
    def go_poweron(self):
        ev = MP_Events.Instance()
        ev.new_event(None, Event_Map.EV_STATUS_CHANGE, Hw_Interface.MP)
        self.mp_status = Hw_Interface.MP_POWER_ON
    '''

    @staticmethod
    def go_wakeup():
        ev = MP_Events.Instance()
        ev.new_event(None, Event_Map.EV_STATUS_CHANGE, Hw_Interface.MP_WAKEUP)
        
    @staticmethod    
    def go_sleep():
        ev = MP_Events.Instance()
        ev.new_event(None, Event_Map.EV_STATUS_CHANGE, Hw_Interface.MP_GOSLEEP)
        
    @staticmethod
    def go_menu():
        ev = MP_Events.Instance()
        ev.new_event(None, Event_Map.EV_STATUS_CHANGE, Hw_Interface.MP_GOMENU)
    
    @staticmethod
    def go_on():
        ev = MP_Events.Instance()
        ev.new_event(None, Event_Map.EV_STATUS_CHANGE, Hw_Interface.MP_GOON) 
        
    @staticmethod
    def go_shutdown():
        ev = MP_Events.Instance()
        ev.new_event(None, Event_Map.EV_STATUS_CHANGE, Hw_Interface.MP_SHUTDOWN) 
    