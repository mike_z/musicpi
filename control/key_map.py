#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from music_pi.mp_events import MP_Events
from music_pi.event_map import Event_Map

from logger import log


class Key_map:
    
    STATE_DOWN  = 1
    STATE_UP    = 2
    
    KEY_PREV = 0
    KEY_NEXT = 1
    KEY_MENU = 2
    KEY_BACK = 3
    KEY_PLAY = 7
    KEY_PFOL = 6
    KEY_NFOL = 5
    KEY_PRW  = 4
    
    def __init__(self):
        
        # mapa akcji przypisana aktualnie do klawiszy
        self.key_map_down = []
        self.key_map_up = []
        for i in range(0, 8): #@UnusedVariable
            self.key_map_down.append(None) # wypełnienie do len()
            self.key_map_up.append(None) # wypełnienie do len()
            
        ev = MP_Events.Instance()
        ev.add_hook(Event_Map.EV_KEY_DOWN, 'KEY_DOWN_READ', self.intercept_keydown)
        ev.add_hook(Event_Map.EV_KEY_UP, 'KEY_UP_READ', self.intercept_keyup)  
        ev.add_hook(Event_Map.EV_KEY_MAP, 'KEY_MAP', self.map_key_callback)  
        ev.add_hook(Event_Map.EV_KEY_MCLEAR, 'KEY_MCLEAR', self.clear_map_callback)  
     
     
    def map_key_callback(self, source, data):
        key = data.get('key', 0)
        fun =  data.get('fun', None)
        state =  data.get('state', 1)
        self.map_key(key, fun, state)
                 
    def map_key(self, key, fun, state=1):
        '''
        Mapowanie statusu przycisku do metody
        W razie wystąpienia zostanie odpalona ta metoda
        '''
        log.debug("Key event registered: " + str(key) + " state: " + str(state))
        if state == self.STATE_UP:
            self.key_map_up[key] = fun
        elif state == self.STATE_DOWN:
            self.key_map_down[key] = fun
    
    def clear_map_callback(self, source, data):
        self.clear_map()
        
    def clear_map(self):
        log.debug('---clear key map')
        for i in range(0, len(self.key_map_up)):
            self.key_map_up[i] = None
        for i in range(0, len(self.key_map_down)):
            self.key_map_down[i] = None   
            
    def intercept_keydown(self, source, data):
        try:
            self.key_map_down[data['key']]()
        except(KeyError):
            log.error('Błąd przekazania data dla key id')
        except(TypeError):
            log.warning('Brak przypisania')
        except:
            log.error('nie udało się wywołać naciśnięcia przycisku')
        
    def intercept_keyup(self, source, data):
        try:
            self.key_map_up[data['key']]()
        except(KeyError):
            log.error('Błąd przekazania data dla key id')
        except(TypeError):
            log.warning('Brak przypisania')
        except:
            log.error('nie udało się wywołać podniesienia przycisku')