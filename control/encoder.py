#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from time import sleep
import threading

from hw.encoder_lib import HwEncoder
from music_pi.mp_events import MP_Events
from music_pi.event_map import Event_Map

from logger import log

class Encoder:
    '''
    Obsługa sygnałów sterowania:
    enkoder
    
    Zależna od pętli sprzętowej głównej aplikacji
    '''    
    
    def __init__(self, config_d):
        
        # Ładowanie konfiguracji z przekazanego słownika
        if not isinstance(config_d, dict):
            log.fatal('missing configuration for control')
            raise Exception('missing configuration for control')
            pass
        else:
            # lista potrzebnych danych
            atr_list = ['ENC_A', 'ENC_B', 'UPDATE_TIME2']
            load_errors = 0
            
            # ładowanie po każdej ze zmiennych
            for atr in atr_list:
                if not atr in config_d:
                    log.fatal('missing {0} configuration for control'.format(atr))
                    raise Exception('missing {0} configuration for control'.format(atr))
                else:
                    setattr(self, atr, config_d[atr])
            
            # Szybkie sprawdzenie czy czegoś nie brakuje
            if load_errors > 0:
                log.fatal('missing configuration')
                log.fatal(config_d)
                raise Exception('missing configuration')
            else:
                log.debug('configuration ok')       
        

        self.enc = HwEncoder(self.ENC_A, self.ENC_B)
                
        #  1 załadowanie
        self.enc.get_result()
            
        # Własna pętla wysyłania i obsugi klawiatury
        self._th_valid = True
        self._th_time = self.UPDATE_TIME2
        self._th_thread = threading.Thread(target=self._enc_loop, name='HW-enc-Read')
        self._th_thread.daemon = True # zakończy się razem z głównym wątkiem
        self._th_thread.start()            
    
    def __del__(self):
        log.debug('__del__ Encoder')
        self._th_valid = False
        self.enc.__del__()
        self._th_thread.join(self.UPDATE_TIME)
        sleep(self.UPDATE_TIME)
        
    def _enc_loop(self):
        '''
        Sprawdzanie statusu flagi przerwania INT
        '''
        while self._th_valid:
            res = self.enc.get_result()
            if res != 0:
                ev = MP_Events.Instance()
                ev.new_event(self, Event_Map.EV_ENC, {'val':res})
                log.debug('EV_ENC {0}'.format(res))

            sleep(self._th_time)
            