#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

#import sys, os
from time import sleep #, strftime
from hw.lcd_lib import lcd_hd44780
from control.layer_record import Layer_Record

#import RPi.GPIO as GPIO #@UnresolvedImport
from logger import log 

import threading

from music_pi.mp_events import MP_Events
from music_pi.event_map import Event_Map
from music_pi.mp_timer import MP_Timer


'''
@todo: fix dla zatrzymania przewijania po przekroczeniu pełnego przewinięcia tekstu
@todo: fix dla duplikatów wpisów
'''

class Display:
    '''
    Sterownik LCD wyższego poziomu
    
    Zapewnia uruchomienie sterowników sprzętowych oraz 
    burofowanie urządzeń przed nadmiernym ruchem na masgistrali 
    i2c. Tekst umieszczany jest za pomocą obsługi prostych konfigurowalnych
    warstw, układanych w liście.
    
    @todo: łamanie wierszy przy przewijaniu w miejscach spacji a nie
            w trakcie zdania
    '''

    # obj. holders
    display_dev = []    # obiekty urządzenień lcd_hd44780
    layers      = []    # warstwy tekstu na wyświetlacz
    row_content = []    # rzeczywista zawartość do wyświetlenia
    row_buff    = []    # duble-buffor

    # formaty
    ALIGN_LEFT      = 0
    ALIGN_CENTER    = 1
    ALIGN_RIGHT     = 2
    SCROLL_TEXT     = 3
    
    SCROLL_TIME     = 1
    
    # dodatkowe znaki generatora
    SYMBOL_ARROW_RIGHT = 0x7E
    SYMBOL_ARROW_LEFT  = 0x7D
        # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # -=-=- Inicjalizacja i basic -=-=- 
    
    def __init__(self, config_d):
        
        log.debug('init display manager')
        
        # Ładowanie konfiguracji z przekazanego słownika
        if not isinstance(config_d, dict):
            log.fatal('missing configuration for display(s)')
            raise Exception('missing configuration for display(s)')
            pass
        else:
            # lista potrzebnych danych
            atr_list = ['LCD_NUM', 'ROW_SIZE', 'ROW_NUM', 
                        'LCD_PCF', 'LCD_PIN_E', 'LCD_PIN_RS', 
                        'UPDATE_TIME']
            load_errors = 0
            
            # ładowanie po każdej ze zmiennych
            for atr in atr_list:
                if not atr in config_d:
                    log.fatal('missing {0} configuration for display(s)'.format(atr))
                    raise Exception('missing {0} configuration for display(s)'.format(atr))
                else:
                    setattr(self, atr, config_d[atr])
            
            # Szybkie sprawdzenie czy czegoś nie brakuje
            if load_errors > 0:
                log.fatal('missing configuration')
                log.fatal(config_d)
                raise Exception('missing configuration')
            else:
                log.debug('configuration ok')
        
        # Tworzenie obitków wyświetlaczy
        for i in range(0, self.LCD_NUM):
            log.debug("init display " + str(i))
            self.display_dev.append(lcd_hd44780(self.LCD_PCF[i], 
                                                self.LCD_PIN_E[i], 
                                                self.LCD_PIN_RS[i])) 
            for j in range(0, self.ROW_NUM): #@UnusedVariable                        
                self.row_content.append("")
                self.row_buff.append("")
                
        
        # Własna pętla wysyłania i obsugi wyświetlacza
        # gwarantuje brak opóźnień względem reszty urządzenia
        self._th_valid = True
        self._th_time = self.UPDATE_TIME
        self._th_thread = threading.Thread(target=self._update_display, name='HW-Disp-Up')
        self._th_thread.daemon = True # zakończy się razem z głównym wątkiem
        self._th_thread.start()
        
        self._scrool_timer = MP_Timer(4, self.scroll_layers)
        self._scrool_timer.start()
        
        ev = MP_Events.Instance()
        ev.add_hook(Event_Map.EV_LCD_PRINT, 'LCD_PRINT', self.print_display_callback)
        ev.add_hook(Event_Map.EV_LCD_CLEAR, 'LCD_CLEAR', self.clear_callback)
        ev.add_hook(Event_Map.EV_LCD_SCROLL_RST, 'LCD_SCROLL_RST', self.scroll_rst_callback)
        
        
        
    def __del__(self):            
        log.debug("__del__ Display ")
        self._scrool_timer.stop()
        self._th_valid = False
        self._th_thread.join(self.UPDATE_TIME)
        self.display_dev = []
        pass
        
            
    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # -=-=- Wejście dla thread -=-=-    
    
    def scroll_layers(self, s, m):
        for layer in self.layers:
            self.update_layer(layer) # uaktualnienie przesunięć
    
    def _update_display(self):
        '''
        Aktualizacja zawartości warstw oraz w razie konieczności sprzętu
        '''
        log.debug('_update_display loop')
        
        while self._th_valid:
        
            # czyszczenie bufora wierszy
            for i in range(0, len(self.row_content)):
                self.row_content[i] = ""
            
            # aktualizacja warstw i buff
            for layer in self.layers:
                self.update_row_content(layer) # uaktualnienie buforów
                
            # zarządzanie buforami           
            for i in range(0, len(self.row_content)):
                
                # upewnienie się że linia ma długość ROW_SIZE
                if len(self.row_content[i]) > self.ROW_SIZE:    # docięcie do max
                    self.row_content[i] = self.row_content[i][:20]
                elif len(self.row_content[i]) < self.ROW_SIZE:  # wypełnienie spacjami
                    self.row_content[i] = ('{:^'+str(self.ROW_SIZE)+'}').format(self.row_content[i])
                
                # czy linia się różni od poprzedniej wartości
                if self.row_content[i] != self.row_buff[i]:
                    self.display_row(self.row_content[i], i)    # wyświetlenie na ekranie
                    self.row_buff[i] = '' + self.row_content[i] # w duble bufferze
            
            sleep(self._th_time)
            
    def display_row(self, string, row):
        '''
        Wysłanie nowej zawartości do wiersza wyświetlacza
        '''
        disp = row / self.ROW_NUM
        disp_row = row - (disp * self.ROW_NUM)
        
        self.display_dev[disp].lcd_display_string(string, disp_row, 0)
        log.debug("display_row # " + string + " @ " + str(row))

    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # -=-=- Warstwy -=-=-     
    
    def update_layer(self, layer):
        '''
        Aktualizacja warstwy
        '''
        
        # dla warstwy scrollowanej dodatkowe sprawdzenie tick
        if layer.style == self.SCROLL_TEXT:
            
            if layer.pos < len(layer.text):
                layer.pos += self.ROW_SIZE
            else:
                layer.pos = 0
                
        '''
        # napis przewijany ale mieści się w polu
        if len(layer.text) < (layer.row_len+1)*self.ROW_SIZE:
            layer.tick = 0
            
        elif layer.tick >= self.SCROLL_TIME:
            layer.tick = 0
        
            if layer.pos < len(layer.text):
                layer.pos += self.ROW_SIZE
            else:
                layer.pos = 0
        
        else:
            layer.tick += 1    
        '''

    def update_row_content(self, layer):
        '''
        Aktualizacja row bufforów po jednej warstwie
        '''
        
        # przewijanie kolejnych ekranów        
        if layer.style == self.SCROLL_TEXT:
            new_rows = self.scroll_style(layer.text, layer.pos, layer.row_len)   
            n_ly = 0 # dla indeksu new_rows
            for i in range(layer.row_start, layer.row_start+len(new_rows)):
                # nakładanie warstw
                self.row_content[i] = self.stack_layer(self.row_content[i], new_rows[n_ly])
                n_ly += 1
        
        # metody wyświetlania statyczne    
        else:
            row_text = self.fixed_align(layer.text, layer.style)
            # nakładanie warstw
            self.row_content[layer.row_start] = self.stack_layer(self.row_content[layer.row_start], row_text)
            
    def stack_layer(self, prev_text, new_text):
        '''
        Nanoszenie warstw z przeźroczystością w miejsce spacji
        '''
        
        # jeśli nie było oryginalnie odpowiedniej długości, np po starcie i czyszczeniu
        if len(prev_text) != self.ROW_SIZE:
            prev_text = ('{:<'+str(self.ROW_SIZE)+'}').format(prev_text)
                
        if len(new_text) != self.ROW_SIZE:
            log.error("Stacked layer text have diffrent size than display row size!")
            return prev_text
        
        # stringi raz utworzone nie mogą być tak łatwo modyfikowane jak tabel,
        # konieczna jest konwersja do listy
                
        result = list(prev_text) # cały string poprzedniej warstwy już w liście
        
        count = 0
        for i in new_text:      # przeglądanie każdego kolejnego znaku w poszukiwaniu spacji
            if i != " ":        # to co nie jest spacją przesłania napis na przodzie
                result[count] = i
            count += 1
            
        return ''.join(result)        
        
    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # -=-=- style -=-=-   
    
    def fixed_align(self, string, align):
        '''
        Generowanie tekstu ustawionego statycznie (align)
        
        Args:
            row: względny wiesz
            align: wyrównanie w poziomie
        
        Returns:
            string: sformatowany tekst z długością self.ROW_SIZE znaków
        
        @todo: marginesy
        '''
        
        # polskie czcionki powodują problem z liczeniem kodów ascii
        string = self.lcd_fix_polish_char(string)
        final_string = string
        
        if align == self.ALIGN_LEFT:        # wyrównanie do lewej
            
            if len(string) > self.ROW_SIZE: # przycięcie rozmiaru z prawej strony
                final_string = string[:self.ROW_SIZE]
                
            #wypełnienie spacjami po prawej stronie
            final_string = ('{:<'+str(self.ROW_SIZE)+'}').format(final_string) 
                
        elif align == self.ALIGN_RIGHT:     # wyrównanie do prawej
            
            if len(string) > self.ROW_SIZE: # przycięcie rozmiaru z lewej strony
                final_string = string[(len(string) - self.ROW_SIZE):self.ROW_SIZE]
                
            #wypełnienie spacjami po lewej stronie stronie
            final_string = ('{:>'+str(self.ROW_SIZE)+'}').format(final_string)
                
        else:                               # centrowanie tekstu
            if len(string) > self.ROW_SIZE: # przycięcie do środka
                center_offset = (len(string) / 2) - (self.ROW_SIZE / 2) 
                final_string = string[center_offset:self.ROW_SIZE]          

            # uzupełnienie spacjami do obu stron
            final_string = ('{:^'+str(self.ROW_SIZE)+'}').format(final_string)
        
        return final_string
    

    def scroll_style(self, string, offset, row_len):
        '''
        Generowanie tekstu z warstwy, z przewijaniem
        
        Args:
            string: ciąg tekstu warstwy
            offset: przesunięcie ciągu znaków od poczatkowej pozycji (offset <= len(string))
            row_len: ile lini ma zajmować tekst 
                (0 tylko ta sama linia, 1 ta i kolejna, itd.)
        
        Returns:
            string: lista, sformatowanych tekstów z długością self.ROW_SIZE znaków. 
                Długość listy jest determinowana parametrem row_len lub maksymalną 
                ilośćą linii jakie zajmuje tekst (gdy krótszy niż zamierzane wypełnienie.
                np. ("tekst bardzo dlugi bedzie pozielony", 0, 1) przy ROW_SIZE = 10
                zwróci: ["tekst bard", "zo dlugi b"]
        '''
                    
        # polskie czcionki powodują problem z liczeniem kodów ascii
        string = self.lcd_fix_polish_char(string)
        
        '''
        @todo: spradzenie danych
        @todo: dzielenie wyrazów lub dzielenie w miejscu spacji
        '''
        
        new_rows = []
        t_offset = offset
        max_row = row_len+1 
        
        # czy zostaną zapełnione wszystkie wiersze?
        if len(string) < max_row*self.ROW_SIZE:
            max_row = (len(string) / self.ROW_SIZE) + 1
        
        for i in range(0,max_row): #@UnusedVariable
            n_str = ""
            
            # jeśli ilość znaków jest zbyt mała żeby zapełnić pełną linię
            if (t_offset+self.ROW_SIZE) > len(string) and t_offset < len(string): 
                n_str = string[t_offset:t_offset+len(string)]
                
            # offset przekroczył długość ciągu, zawinięcie z początkiem napisu
            elif t_offset > len(string):
                t_offset = 0
                n_str = string[t_offset:t_offset+self.ROW_SIZE]
                
            # offset pozawala na cały ciąg z stringa
            else:
                n_str = string[t_offset:t_offset+self.ROW_SIZE]
            
            # wypełnie spacjami, dla pewności długości ciagu
            n_str = ('{:<'+str(self.ROW_SIZE)+'}').format(n_str) 
            
            # ostatnia linia ciagu i jest przewijana
            if i == row_len and len(string) >= max_row*self.ROW_SIZE:
                symbol = chr(self.SYMBOL_ARROW_RIGHT) # symbol strzałki
                cut_str = n_str[:self.ROW_SIZE-1]
                n_str = ''.join([cut_str, symbol] )
                
            new_rows.append(n_str) # dodanie do listy
            
            t_offset += self.ROW_SIZE # przeskok o szerokość wyświetlacza

        return new_rows


    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # -=-=- Wpisywanie testu -=-=- 
    
    #def get_layer(self, string, style=ALIGN_LEFT, start_row=0, row_len=0):
        '''
        Umieszcza tekst na ekranie (w buforze warstw)
        
        Args:
            string: ciąg do wyświetlenia, jeśli dłuższy niż wiersz 
                podlega metodą zależnym od stylu
            style: styl formatowania lub przewijania, tj. display.ALIGN_LEFT, display.ALIGN_CENTER,
                display.ALIGN_RIGHT, display.SCROLL_TEXT
            start_row: numer wiersza początkowego
            row_len: (0 = ten sam wiersz) ile wierszy
        '''
        
    #    new_layer = Layer_Record(string, style, start_row, row_len)
    #    #self.layers.append(new_layer)
    #    return new_layer
        
    # punkt wejścia 
    def print_display_callback(self, source, data):
        '''
        Umieszcza w buforze tekst
        '''
        self.layers.append(data)
        #return record
    
    def clear_callback(self, s, d):
        '''
        Czyści wszystkie warstwy z tekstu
        '''
        log.debug('clear display layers')
        if len(self.layers) == 0:   #pusta lista nie może wykonać pop
            return                  
        
        #for layer in self.layers.pop():
        #    del layer # hah zapomnij o takim del w pythonie ;-)
        #del self.layers[:]
        self.layers = []
        
    def scroll_rst_callback(self, s, d):
        for layer in self.layers:
            layer.tick = 0
            layer.pos = 0
        
        pass
    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # -=-=-  -=-=-   
    def lcd_fix_polish_char(self, string):
        """
        Naprawienie polskich znaków z czcionek dostępnych w wyświetlaczu 
        z kas UPOS (przesunięte czcionki względem standardu ASCII)
        
        Args:
            string: ciąg do poprawy znaków
        
        Returns:
            string: ciąg z zmienionymi znakami prawidłowo wyświetlanymi
        """
        string = string.replace("ą", chr(0x9F))
        string = string.replace("Ą", chr(0x9F))
        string = string.replace("ć", chr(0xAD))
        string = string.replace("Ć", chr(0xAD))
        string = string.replace("ę", chr(0xAF))
        string = string.replace("Ę", chr(0xAF))
        string = string.replace("ł", chr(0xA0))
        string = string.replace("Ł", chr(0xA0))
        string = string.replace("ń", chr(0xB3))
        string = string.replace("Ń", chr(0xB3))
        string = string.replace("ó", chr(0x93))
        string = string.replace("Ó", chr(0x93))
        string = string.replace("ś", chr(0xA2))
        string = string.replace("Ś", chr(0xA2))
        string = string.replace("ź", chr(0xA7))
        string = string.replace("Ź", chr(0xA7))
        string = string.replace("ż", chr(0xA9))
        string = string.replace("Ż", chr(0xA9))    
        return string   
    
        