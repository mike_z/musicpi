#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from hw import i2c_lib
from logger import log

from music_pi.mp_events import MP_Events
from music_pi.event_map import Event_Map

from hw.pwm_lib import PWM_PCA9633

class PWM:
    '''    
    @todo: dodać możliwość migania diodami, lub pulsowania
    @todo: ustawianie selektywne jednego z wyjść
    @todo: konfiguracja słowna według dokumentacji
    '''
    
    def __init__(self, config_d):
        
        # Ładowanie konfiguracji z przekazanego słownika
        if not isinstance(config_d, dict):
            log.fatal('missing configuration for pwm')
            raise Exception('missing configuration for pwm')
            pass
        else:
            # lista potrzebnych danych
            atr_list = ['PWM_ADR', 'PWM_LCD0', 'PWM_LCD1', 'PWM_KEY', 'PWM_ENC']
            load_errors = 0
            
            # ładowanie po każdej ze zmiennych
            for atr in atr_list:
                if not atr in config_d:
                    log.fatal('missing {0} configuration for pwm'.format(atr))
                    raise Exception('missing {0} configuration for pwm'.format(atr))
                else:
                    setattr(self, atr, config_d[atr])
            
            # Szybkie sprawdzenie czy czegoś nie brakuje
            if load_errors > 0:
                log.fatal('missing configuration')
                log.fatal(config_d)
                raise Exception('missing configuration')
            else:
                log.debug('configuration ok') 
        
        # init
        self.pwm_dev = PWM_PCA9633(self.PWM_ADR, self.PWM_LCD0, self.PWM_LCD1, self.PWM_KEY, self.PWM_ENC)
        
        ev = MP_Events.Instance()
        ev.add_hook(Event_Map.EV_PWM_SET, 'PWM_SET', self.set_callback)
        
    def __del__(self):
        log.debug("__del__ pwm_PCA9633 ")
        self.pwm_dev.__del__()
        pass
    
    def set_callback(self, source, data):
        '''
        Ustawia wyjścia pwm na wskazane wartosci 
        
        zakres 0-255
        
        Args:
            data[0] - wyświetlacz górny
            data[1] - wyświetlacz dolny
            data[2] - podświetlanie przycisków
            data[3] - podświetlanie gałki
        '''
        if len(data) != 4:
            log.fatal('Zła ilość parametrów != 4')
            return
            
        self.LED0     = data[0]
        self.LED1     = data[1]
        self.LED2     = data[2]
        self.LED3     = data[3]
        
        # bez minimalnego podświetlania ekranów tekst nie będzie widoczny
        if self.LED0 == 0 or self.LED1 == 0:
            self.LED0 = 1 
            self.LED1 = 1
            
        self.pwm_dev.update_pwm(self.LED0, self.LED1, self.LED2, self.LED3)        
        