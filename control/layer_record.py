#!/usr/bin/python
# -*- coding: utf-8 -*-

class Layer_Record:


    def __init__(self, 
                 layer_text='', 
                 layer_style_type=0, 
                 layer_row_start=0, 
                 layer_row_len=1):

        self.text = layer_text
        
        self.style = layer_style_type

        self.row_start = layer_row_start
        self.row_len = layer_row_len

        self.tick = 0
        self.pos = 0
        
