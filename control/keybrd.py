#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from time import sleep
import threading

from hw.key_lib import Keybrd_PCF8574
from music_pi.mp_events import MP_Events
from music_pi.event_map import Event_Map

from logger import log

class Keybrd:
    '''
    Obsługa sygnałów sterowania:
    klawisze, enkoder
    
    Zależna od pętli sprzętowej głównej aplikacji
    '''
    
    STATE_DOWN  = 1
    STATE_UP    = 2
    
    
    def __init__(self, config_d):
        
        # Ładowanie konfiguracji z przekazanego słownika
        if not isinstance(config_d, dict):
            log.fatal('missing configuration for control')
            raise Exception('missing configuration for control')
            pass
        else:
            # lista potrzebnych danych
            atr_list = ['KEY_PCF', 'KEY_INT', 'UPDATE_TIME']
            load_errors = 0
            
            # ładowanie po każdej ze zmiennych
            for atr in atr_list:
                if not atr in config_d:
                    log.fatal('missing {0} configuration for control'.format(atr))
                    raise Exception('missing {0} configuration for control'.format(atr))
                else:
                    setattr(self, atr, config_d[atr])
            
            # Szybkie sprawdzenie czy czegoś nie brakuje
            if load_errors > 0:
                log.fatal('missing configuration')
                log.fatal(config_d)
                raise Exception('missing configuration')
            else:
                log.debug('configuration ok')       
        

        self.keybrd = Keybrd_PCF8574(self.KEY_PCF, self.KEY_INT)
                
        # poprzednie odczytane statusy portów, 1 załadowanie
        self.old_status = self.keybrd.read_io()
        
        # mapa akcji przypisana aktualnie do klawiszy
        self.key_map_down = []
        self.key_map_up = []
        for i in range(0, 8): #@UnusedVariable
            self.key_map_down.append(None) # wypełnienie do len()
            self.key_map_up.append(None) # wypełnienie do len()
            
            
        # Własna pętla wysyłania i obsugi klawiatury
        self._th_valid = True
        self._th_time = self.UPDATE_TIME
        self._th_thread = threading.Thread(target=self._keys_loop, name='HW-Keys-Read')
        self._th_thread.daemon = True # zakończy się razem z głównym wątkiem
        self._th_thread.start()            
            
        #self.encoder = encoder_lib.HwEncoder(self.ENC_A, self.ENC_B)
    
    def __del__(self):
        log.debug('__del__ Keybrd')
        self._th_valid = False
        self._th_thread.join(self.UPDATE_TIME)
                
        
    def _keys_loop(self):
        '''
        Sprawdzanie statusu flagi przerwania INT
        '''
        while self._th_valid:
            if self.keybrd.get_status():
                # Zmiana wymusza uruchomienie zmapowanej akcji
                self.interpret_status(self.keybrd.read_io())
            sleep(self._th_time)
            
    def interpret_status(self, status_list):
        '''
        Interpretacja zmiany satatusu - odczyt danych oraz 
        przełożenie wartości bitów na to czy przycisk został 
        naciśnięty czy też puszczony oraz jaki to przycisk
        
        Args:
            status_list: lista nowych statusów
        '''
        
        if len(status_list) != 8:
            log.fatal('keys status need be 8 bit long to work!')
            raise Exception('keys status need be 8 bit long to work!')
            pass
            
        # przeglądanie listy w poszukiwaniu zmian
        for i in range(0,8):
        
            # wpis różny a więc zaszła akcja
            if status_list[i] != self.old_status[i]:
                
                ev = MP_Events.Instance()
                
                # wywołanie odpowiedniego mapowania
                if status_list[i] == True: # został naciśnięty
                    ev.new_event(self, Event_Map.EV_KEY_DOWN, {'key':i})
                    log.debug('push_down {0}'.format(i))
                else:                      # został puszczony
                    ev.new_event(self, Event_Map.EV_KEY_UP, {'key':i})
                    log.debug('push_up {0}'.format(i))
            
            # rejestrowanie statusu
            self.old_status[i] = status_list[i] 
            