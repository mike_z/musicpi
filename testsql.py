#!/usr/bin/python
# -*- coding: utf-8 -*-

import MySQLdb #@UnresolvedImport 
import datetime 

# DB connetor

class SQL_Connection:
    _instance = None
    db = None
    e_code = 0
    
    SENSOR_DATA_TABLE = "pism_data"
    MAX_SENSOR = 1
    MAX_DATA = 2
    
    
    def __init__(self):
        self.db = MySQLdb.connect("localhost","pism","pism","pism" )
        pass
        
    @staticmethod
    def Instance():
        if SQL_Connection._instance == None:
            SQL_Connection._instance = SQL_Connection()
        
        return SQL_Connection._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)  
    
    def __del__(self):
        self.db.close()
          
    #-----------------------

    def check_connection(self):
        pass
    
    def execute(self, sql_querry=None):
        if sql_querry is None:
            self.e_code = 2
            return False
        else:
            cursor = self.db.cursor()
            
            try:
                # Execute the SQL command
                cursor.execute(sql_querry)
                # Commit your changes in the database
                self.db.commit()
                self.e_code = 0
                return True
            except:
                # Rollback in case there is any error
                self.db.rollback()
                self.e_code = 1
                return False
            
    def select(self, sql_querry=None):
        if sql_querry is None:
            self.e_code = 2
            return None
        else:
            cursor = self.db.cursor(MySQLdb.cursors.DictCursor)
            
            cursor.execute(sql_querry)
            return cursor
        
        #cursor.fetchall()
        #cursor.fetchone()
        #cursor.rowcount



    def add_data_record(self, sensor_id, data):
        

        
        if sensor_id > self.MAX_SENSOR or sensor_id < 0:
            return False
        
        if len(data) > self.MAX_DATA or len(data) == 0:
            return False
               
        sqlq = "INSERT INTO " + self.SENSOR_DATA_TABLE + " ("
        sqlq += "czujnik"
        
        for i in range(0, len(data)):
            sqlq += ",data" + str(i)
        
        sqlq += ") VALUES ("
        sqlq += str(sensor_id)
        
        for i in range(0, len(data)):
            sqlq += ",'" + str(data[i]) + "'"
            
        sqlq += ");"
        #print str(sqlq)
        self.execute(sqlq)
        print str(self.e_code)
        
    def get_data_records(self):
        
        sqlq = "SELECT id,czas,czujnik"
        
        for i in range(0, self.MAX_DATA):
            sqlq += ",data" + str(i)
         
        sqlq += " FROM " + self.SENSOR_DATA_TABLE + " "
        
        #print str(sqlq)
        cursor = self.select(sqlq)
        data = cursor.fetchall()
        return data
        
    def read_record(self, sql_record):
        pass
        
        
        
sqlcon = SQL_Connection.Instance()

#sqlcon.add_data_record(0, [12.123,21.145])

rows = sqlcon.get_data_records()
for row in rows:
    rec = "id:" + str(row['id']) + " czas:" + str(row['czas']) + " czujnik:" + str(row['czujnik'])
    
    for i in range(0,2):
        rec += " data" + str(i) + ":" + str(row['data'+str(i)])
        
    print rec    
        
        

