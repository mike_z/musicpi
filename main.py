#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

'''
Moduł startowy aplikacji odtwarzacza

@todo: wyłapywanie wyjątku aplikacji żeby ją zrestartować
'''

from time import sleep
import music_pi
import sys
import coloredlogs, logging #@UnresolvedImport

from music_pi.mp_events import MP_Events
from music_pi.mp_hook import MP_hook
from music_pi.mp_msg import MP_msg

# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# -=-=- LOGGER -=-=-  
'''
Do logowania zdarzeń zaimplementowano wbudowany logging z dodatkiem
coloredlogs autoamtycznie kolorującego informacje według schetu.

Uwaga! wymaga zainstalowania coloredlogs przez pip install
Wymaga konfiguracji loggera poprzez coloredlogs.install z trybem poziomu 
logowania stylami itd. (jeśli nie są domyślne)
'''

# dla standardowego białego logu to
#logging.basicConfig(format='%(asctime)s - (%(threadName)s) - %(pathname)s - %(levelname)s - %(message)s', level=logging.DEBUG)

# kolory dla cześci wpisu
FIELD_STYLES = dict(
    asctime=dict(color='green'),
    threadName=dict(color='magenta'),
    pathname=dict(color='cyan'),
    levelname=dict(color='white', bold=coloredlogs.CAN_USE_BOLD_FONT),
    name=dict(color='blue'))

# kolory dla poziomów informacji
LEVEL_STYLES = dict(
    debug=dict(color='green', bold=coloredlogs.CAN_USE_BOLD_FONT),
    info=dict(color='white'),
    verbose=dict(color='blue'),
    warning=dict(color='yellow', bold=coloredlogs.CAN_USE_BOLD_FONT),
    error=dict(color='red'),
    critical=dict(color='red', bold=coloredlogs.CAN_USE_BOLD_FONT))

# przypisanie poziomu logownia i map kolorów # DEBUG
coloredlogs.install(level='INFO', 
                    fmt='%(asctime)s - (%(threadName)s) - %(pathname)s - %(levelname)s - %(message)s',
                    level_styles = LEVEL_STYLES,
                    field_styles = FIELD_STYLES)

# główny logger to root
log = logging.getLogger('root')

'''
w innych modułach wykorzystywany jest plik logger.py który zawiera:

 import logging
 log = logging.getLogger('root')

wykorzystanie:

 from logger import log

w efekcie otrzymany zostanie głowny logger dla calej aplikacji

''' 

# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# -=-=- Global zmienne -=-=-   
APP = dict()
APP['NAME']     = "Music Pi"
APP['VER']      = "0.1"
APP['DATE']     = '2016-03-28'
APP['AUTHOR']   = "Michał Żak"


ST = dict()
ST['AUTOSTART'] = True
ST['PWM_DISP_PLAY'] = 2
ST['PWM_DISP_STBY'] = 0
ST['PWM_KEY_PLAY'] = 0
ST['PWM_KEY_STBY'] = 0
ST['VOL'] = 95


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# -=-=- system message fifo -=-=-   
        
IS_ALIVE = True    

# aplikacja uruchomiona jako główny plik
if __name__ == '__main__':
    

    log.info('{0} {1} {2}'.format(APP['NAME'], APP['VER'] , APP['AUTHOR']))
    
    ev = MP_Events.Instance()
    main_app = music_pi.music_pi()   
    
    '''
    logging.debug("this is a debugging message")
    logging.info("this is an informational message")
    logging.warn("this is a warning message")
    logging.error("this is an error message")
    logging.critical("this is a critical message")
    '''
    
    log.debug("main_app loop")
    
    MAIN_APP_TIMEOUT = 1 
    
    # obsługa opuszczenia wątków po próbie zabicia procesu Ctrl+C
    while IS_ALIVE:
        try:
            #ev.event_loop()
            if main_app.is_alive() == False:
                sys.exit()
            
            sleep(MAIN_APP_TIMEOUT)
            pass
        
        except (KeyboardInterrupt, SystemExit):
            
            logging.info('Przerwanie CTRL+C, wyjście z wątków')
            
            #del main_app 
            #main_app.__del__()
            logging.info('Opuszczenie aplikacji')
            sys.exit()
    
    log.info('QUIT')