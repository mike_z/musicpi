#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

'''
Prosta procedurka wyświetlenia w czytelny sposób wartości binarnej z n pozycjami
'''
get_bin = lambda x, n: x >= 0 and "0b"+str(bin(x))[2:].zfill(n) or "-"+"0b"+str(bin(x))[3:].zfill(n)
