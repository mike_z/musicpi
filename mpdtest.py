#!/usr/bin/python
# -*- coding: utf-8 -*-

from mpdc import MpdClient, MPDError, CommandError 
import time 
import datetime 
from time import sleep
#import urllib


import mpdc
     

def humanize_time(secs):
    secs = int(secs.partition(".")[0])
    mins, secs = divmod(secs, 60)
    hours, mins = divmod(mins, 60)
    secs = str(secs).zfill(2)
    mins = str(mins).zfill(2)
    hours = str(hours).zfill(2)
    return '%sh%sm%ss' % (hours, mins, secs)
  
  
def format_song(song):
    """Formatte joliment un fichier"""
    artiste = song["artist"] if "artist" in song.keys() else "<unkown>"
    titre = song["title"] if "title" in song.keys() else "<unkown>"
  
    if artiste == "<unkown>" and titre == "<unkown>":
        return "%s. %s" % (song["pos"], song["file"])
    else:
        return "%s. %s - %s" % (song["pos"], artiste, titre)
  


def mpdStats():
    def dt(u): return datetime.datetime.fromtimestamp(u)
    def ut(d): return time.mktime(d.timetuple())
    u = int(client.stats()['db_update'])

    print("Artists:      " + client.stats()['artists'])
    print("Albums:       " + client.stats()['albums'])
    print("Songs:        " + client.stats()['songs'])
    print(" ")
    print("Play Time:    " + str(datetime.timedelta(seconds=int(client.stats()['playtime']))))
    print("Uptime:       " + str(datetime.timedelta(seconds=int(client.stats()['uptime']))))
    print("DB Updated:   " + str(dt(u)))
    print("DB Play Time: " + str(datetime.timedelta(seconds=int(client.stats()['db_playtime']))))


client = MpdClient.Instance()
#st = client._client.status()
#client.playid(st['songid'])
#client._client.play()



##client.get_devices()

#client.start_idle_threat()

#print(client._client.idle("database", "update"))
#print(client._client.noidle())


print(client._client.status())
sleep(0.5)



#client = MPDClient()               # create client object
#client.timeout = 10                # network timeout in seconds (floats allowed), default: None
#client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
#client.connect("localhost", 6600)  # connect to localhost:6600


#print(client._client.update())

#print(client.mpd_version)          # print the MPD version
#print(client.find("any", "house")) # print result of the command "find any house"
#print(client.currentsong())
#print(client.status())

#client.setvol(70)
#print(client.status())
#print('-------------------------------------------------------')
#print(client.playlist())
#print(client.add('file:///musicpi/test'))
#client.playlistadd("test", "")
#print("Update: " + client.update())
#print(client.listall())
#print(client.list('file'))
#print('-------------------------------------------------------')
#mpdStats()
#file, directory, playlist
#print(client._client.lsinfo("usb0"))

#for entry in client._client.lsinfo("/"):
#    print("%s" % entry['file'])
   


#client.close()                     # send the close command
#client.disconnect()                # disconnect from the server
