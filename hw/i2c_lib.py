#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

import smbus #@UnresolvedImport
#from time import sleep


class i2c_device:
	"""
	Wraper obsługi python-SMBus
	
	(biblioteka w celu pominięcia podawania kolejny raz tego 
	samego adresu urządenia oraz autouzupełniania w PyDev) 
		
	oryginalna biblioteka: https://github.com/the-raspberry-pi-guy/lcd/blob/master/i2c_lib.py
	"""
	
	def __init__(self, addr, port=1):
		"""
		Uruchomienie magistrali na wskazanym porcie
		Dla:	Raspberry Pi model A będzie 0
				Raspberry Pi model B/B+ bedzie 1
		
		Args:
			addr: adres wskazany przez i2c-tools -y port
			port: 0 dla model A, 1 dla B/B+
		"""
		
		self.addr = addr
		self.bus = smbus.SMBus(port)


	def write_cmd(self, cmd):
		"""
		Zapis pojedyńczego bajtu
		
		Args:
			cmd: komenda (byte)
		"""
		
		self.bus.write_byte(self.addr, cmd)
		#sleep(0.0001)
		

	def write_cmd_arg(self, cmd, data):
		"""
		Zapis bajtu z dodatkowym bajtem argumentu
		
		Args:
			cmd: komenda (byte)
			data: dodatkowe informacje byte
		"""
		
		self.bus.write_byte_data(self.addr, cmd, data)
		#sleep(0.0001)

	
	def write_block_data(self, cmd, data):
		"""
		Zapis bajtu z blokiem danych
		
		Args:
			cmd: komenda (byte)
			data: dodatkowe informacje (blok danych)
		"""
		
		self.bus.write_block_data(self.addr, cmd, data)
		#sleep(0.0001)
		
	def write_word_data(self, cmd, data):
		"""
		Zapis bajtu z blokiem danych
		
		Args:
			cmd: komenda (byte)
			data: dodatkowe informacje (blok danych)
		"""
		
		self.bus.write_word_data(self.addr, cmd, data)
		#sleep(0.0001)
		

	def read(self):
		"""
		Odczyt jednego bajtu
		"""
		return self.bus.read_byte(self.addr)

	
	def read_data(self, cmd):
		"""
		Odczyt jednego bajtu z przesłaniem bajtu
		
		Args:
			cmd: komenda (byte)
		"""
		return self.bus.read_byte_data(self.addr, cmd)


	def read_block_data(self, cmd):
		"""
		Odczyt danych blokowych po wyslaniu bajtu 
		
		Args:
			cmd: komenda (byte)
		"""
		return self.bus.read_block_data(self.addr, cmd)
	
	def read_word_data(self, cmd):
		"""
		Odczyt danych blokowych po wyslaniu bajtu 
		
		Args:
			cmd: komenda (byte)
		"""
		return self.bus.read_word_data(self.addr, cmd)
	
	
	def block_process_call(self, cmd, vals):
		return self.bus.block_process_call(self.addr,cmd,vals) 
	
	def write_i2c_block_data(self, cmd, val):
		return self.bus.write_i2c_block_data(self.addr, cmd, val)
	
	def read_i2c_block_data(self, cmd):
		return self.bus.read_i2c_block_data(self.addr, cmd)
