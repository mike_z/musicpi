#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''


from time import sleep
import threading
import RPi.GPIO as GPIO #@UnresolvedImport

class HwEncoder:
    
    def __init__(self, pin_header_a, pin_header_b, reverse_rotate=False):
        
        self.pina = pin_header_a
        self.pinb = pin_header_b
        
        self.reverse = reverse_rotate
        
        GPIO.setmode(GPIO.BOARD)    # numery jak piny w złączu 
        GPIO.setwarnings(False)     # ukrywa "already in use"
        
        GPIO.setup(self.pina, GPIO.IN)
        GPIO.setup(self.pinb, GPIO.IN)
        
        self.last_a = 0
        self.last_b = 0
        
        self.position = 0
        
        self._enc_thread_valid = True
        self.probe_thread = threading.Thread(target=self.probe_loop, name="Encoder thread")
        self.probe_thread.daemon = True
        self.probe_thread.start()
        
    def __del__(self):
        self._enc_thread_valid = False
        self.probe_thread.join(0.002)
        
    def probe_loop(self):
        while self._enc_thread_valid:
            self.position += self.read_encoder()
            sleep(0.001)    
        
    
    def read_input(self):
        sig_a = GPIO.input(self.pina)
        sig_b = GPIO.input(self.pinb)
        
        return [sig_a, sig_b]
    
    def read_encoder(self):
        
        sig_a = GPIO.input(self.pina)
        sig_b = GPIO.input(self.pinb)
        
        #print("sig_a: "+str(sig_a)+", last_a: "+str(self.last_a)+", sig_b "+str(sig_b)+", last_b: "+str(self.last_b))
        
        e_move = 0
        
        if (self.last_a == 0) & (sig_a == 1):
            if sig_b == 0:
                e_move = 1
            else: 
                e_move = -1
        #elif (self.last_b == 1) & (sig_b == 0):
        #    if sig_a == 0:
        #        e_move = 1
        #    else: 
        #        e_move = -1
        else:
            e_move = 0
        
        self.last_a = sig_a
        self.last_b = sig_b
        
        return e_move
    
    def get_result(self):
        result = self.position + 0
        self.position = 0
        return result
