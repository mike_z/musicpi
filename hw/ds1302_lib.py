#!/usr/bin/python
# -*- coding: utf-8 -*-


from time import sleep
from datetime import datetime
import RPi.GPIO as GPIO #@UnresolvedImport
#get_bin = lambda x, n: x >= 0 and "0b"+str(bin(x))[2:].zfill(n) or "-"+"0b"+str(bin(x))[3:].zfill(n)


class DS1302:
    '''
    Sterownik układu DS1302, software-owy
    
    @author: Michał Żak
    @contact: 
    
    Sterownik obsługuje układ Real Time Clock DS1302 z poziomu oprogramowania. 
    Praktycznie dowolne 3 wolne linie Rasberry Pi mogą zostać wykorzysatne 
    do komunikacji. 
    
    ! Przy pierwszym uruchomieniu (zasileniu) należy sprawdzić czy układ 
    ! nie jest zabepieczony przed zapisem is_clock_writeprotected oraz 
    ! czy nie jest zatrzymany is_clock_stoped. Odpwiednio odblokować zegar 
    ! aby mógł normalnie pracować.
    
    @todo: dostęp do ram
    @todo: Flagi Trickle Charger Resistor and Diode Select
    @todo: przeliczanie dziesiątek godzin przy zmianie 24h na AM/PM
    '''
    
    T_SCLK = 0.00025 #~250us
    
    # half-bytes (5-bit actualy)
    HB_STATUS   = 0x0   # 0x81 dla read, 0x80h write, 
    HB_SECONDS  = 0x0   # używając set_command_byte
    HB_MINUTES  = 0x1
    HB_HOUR     = 0x2   # również 12/^24 oraz AM/PM
    HB_DATE     = 0x3
    HB_MONTH    = 0x4
    HB_DAY      = 0x5   # numer dnia tygodnia - day of week number
    HB_YEAR     = 0x6
    HB_WP       = 0x7   # write protect z maską 0xA
    
    HB_BURST    = 0x1F  # burst mode używając set_command_byte
    
    def __init__(self, pin_io, pin_sclk, pin_ce):
        '''
        Sterownik układu DS1302, software-owy
        
        Args:
            pin_io: pin sterujący linią I/O
            pin_sclk: pin sterujący linią SCLK
            pin_ce: pin sterujący linią CE
            (Wszystkie piny numerowane wg. numeru na złączu 40-pin)
        '''
        self.io = pin_io
        self.sclk = pin_sclk
        self.ce = pin_ce
        
        GPIO.setmode(GPIO.BOARD)    # numery jak piny w złączu 
        GPIO.setwarnings(False)     # ukrywa "already in use"
        
        GPIO.setup(self.io,     GPIO.OUT)    # jako wyjście, tymczasowo
        GPIO.setup(self.sclk,   GPIO.OUT)
        GPIO.setup(self.ce,     GPIO.OUT)
        
        GPIO.output(self.io,    False)        # stan niski (LOW: 0)
        GPIO.output(self.sclk,  False)
        GPIO.output(self.ce,    False)
        
    
    def clock_writeprotect(self, wp_on=False):
        '''
        Write protection bit 
        Zabezpiecza układ przed zapisem, można go pozostawić 
        po ustawieniu godziny aby nie zmienić jej nieumyślnie później przez przypadkowe
        śmieci na liniach. Przed zaspiem konieczne jest usunięcie. 
        Domyślnie False
        
        Args:
            wp_on: True/False ochrona danych w układzie przed zapisem
        '''
        if wp_on:
            self.write_single_byte(self.cmd_byte(address=self.HB_WP, write=True), 0x80)
        else:
            self.write_single_byte(self.cmd_byte(address=self.HB_WP, write=True), 0x00)
            
        
    def is_clock_writeprotected(self):
        '''
        Sprawdza czy układ ma włączoną blokadę zapisu
        
        Returns:
            (Boolean) True - układ zabezpieczony przed zapisem
        '''
        clk_data = self.read_single_byte(self.cmd_byte(address=self.HB_WP, write=False)) #@UnusedVariable
        
        if (clk_data & 0x80) == 0:
            return False
        else:
            return True
        
        
    def clock_halt(self, stop_clock=False):
        '''
        Zatrzymanie pracy zegara
        Domyślnie bit właczony po uruchomieniu układu, gdy True układ przestaje liczyć czas 
        i pracuje w trybie standby. Czas nie zostaje skasowany, może służyć jako zatrzymanie 
        minutnika/stopera.
        '''
        #poprzednia ilość sekund, aby nie utracić wartości
        old_data = self.read_single_byte(self.cmd_byte(address=self.HB_STATUS, write=False))
        
        if stop_clock:
            self.write_single_byte(self.cmd_byte(address=self.HB_STATUS, write=True), 
                                   (old_data | 0x80))
        else:
            self.write_single_byte(self.cmd_byte(address=self.HB_STATUS, write=True), 
                                   (old_data & ~0x80))
    
    
    def is_clock_stoped(self):
        '''
        Sprawdza czy zegar jest w zatrzymany
        
        Returns:
            (Boolean) True - zatrzymany
        '''
        clk_data = self.read_single_byte(self.cmd_byte(address=self.HB_STATUS, write=False)) #@UnusedVariable
        
        if (clk_data & 0x80) == 0:
            return False
        else:
            return True
    
    
    def clock_24h(self, time_24=True):   
        '''
        Ustawia zegar w trybie pracy 24h lub AM/PM
        
        Args:
            time_24: True praca w tybie 24h
        
        @TODO poprawka godziny (przeliczenie)
        '''
        old_data = self.read_single_byte(self.cmd_byte(address=self.HB_HOUR, write=False))
        
        if ((old_data & 0x80) == 0) != time_24:
            if time_24:
                self.write_single_byte(self.cmd_byte(address=self.HB_HOUR, write=True), 
                                   (old_data & ~0x80))
            else:
                self.write_single_byte(self.cmd_byte(address=self.HB_HOUR, write=True), 
                                   (old_data | 0x80))
            
                        
    def get_datetime(self):
        '''
        Pobiera cały ciąg wzorca czasu z urzadenia i zwraca jako Datetime
        
        Returns:
            (datetime) wzorzec czasu według układu DS1302
        '''
        burst_data = self.read_burst(self.cmd_byte(address=self.HB_BURST, write=False), 7)
        
        second  = (burst_data[0] & 0xF) + (((burst_data[0] >> 4) & 0x7) * 10) # xSSSssss
        minute  = (burst_data[1] & 0xF) + (((burst_data[1] >> 4) & 0x7) * 10) # xMMMmmmm
        hour    = (burst_data[2] & 0xF) + (((burst_data[2] >> 4) & 0x3) * 10) # toHHhhhh
        date    = (burst_data[3] & 0xF) + (((burst_data[3] >> 4) & 0x3) * 10) # ooDDdddd
        month   = (burst_data[4] & 0xF) + (((burst_data[4] >> 4) & 0x1) * 10) # oooMmmmm
        #day     = (burst_data[5] & 0x7)                                       # oooooDDD
        year    = (burst_data[6] & 0xF) + (((burst_data[6] >> 4) & 0xF) * 10) + 2000 # YYYYyyyy
        
        return datetime(year, month, date, hour, minute, second)         

    
    def set_datetime(self, datetime_stamp):
        '''
        Ustawia czas w układzie DS1302 według przesłanego wzorca czasu
        
        Args:
            datetime_stamp: (datetime) wzorzec czasu
        '''
        dts = datetime_stamp
        
        second_byte = ((dts.second/10) << 4) | (dts.second % 10)
        minute_byte = ((dts.minute/10) << 4) | (dts.minute % 10)
        hour_byte   = ((dts.hour/10) << 4)   | (dts.hour % 10)
        
        date_byte   = ((dts.day/10) << 4)    | (dts.day % 10)
        month_byte  = ((dts.month/10) << 4)  | (dts.month % 10)
        year_byte   = (((dts.year-2000)/10) << 4) | ((dts.year-2000) % 10)
        
        self.write_single_byte(self.cmd_byte(address=self.HB_SECONDS, write=True), second_byte)
        self.write_single_byte(self.cmd_byte(address=self.HB_MINUTES, write=True), minute_byte)
        self.write_single_byte(self.cmd_byte(address=self.HB_HOUR, write=True), hour_byte)
        
        self.write_single_byte(self.cmd_byte(address=self.HB_DATE, write=True), date_byte)
        self.write_single_byte(self.cmd_byte(address=self.HB_MONTH, write=True), month_byte)
        self.write_single_byte(self.cmd_byte(address=self.HB_YEAR, write=True), year_byte)
        
        
    def cmd_byte(self, ram_access=False, address=0x0, write=False):
        '''
        Tworzy bajt polecenia w poprawnej kolejności MSB - LSB (bit 7 do 0)
        
        Args:
            ram_access: False - data/czas, True - RAM
            address: 0x00 do 0x1F
            write: False - odczyt, True - zapis
        
        Returns:
            (Byte): komendę ze wskazanych parametrów
            
            cmd_byte(ram_access=False, address=0x0, write=False) = 0x81
            cmd_byte(ram_access=False, address=0x0, write=True)  = 0x80
            
            self.cmd_byte(ram_access=False, address=self.HB_SECONDS, write=False) = 0x81
        '''
                
        # Zapis 0 spowoduje wyłączenie zapisu do urządzenia
        # a tego nie chcemy
        register = 0x1
        
        register = register << 1
        
        # 0 cloc/calendar, 1 ram
        if ram_access:
            register |= 0x1
        
        register = register << 5
        
        # komenda
        address &= 0x1F # tylko 5 bity
        register |= address
        
        register = register << 1
        
        # 0 write, 1 read
        if not write:
            register |= 0x1
            
        return register
        
        
    def send_byte(self, byte_data):
        '''
        Wysyła bajt do DS1302
        
        Bity transmisji są odwrócone automatycznie LSB -> MSB (bit 0 do 7)
        
        Args:
            byte_data: Byte danych
        '''
        GPIO.setup(self.io, GPIO.OUT)    
        GPIO.output(self.io, False)        
        
        t_byte = byte_data                  
        i = 0
        while i <= 7:
            i+=1

            sleep(self.T_SCLK)
            
            if (t_byte & 0x1) == 0x1:       # transmisja od LSB
                GPIO.output(self.io, True)
            else:
                GPIO.output(self.io, False)
                
            t_byte = t_byte >> 1            # do następnego bitu
        
            GPIO.output(self.sclk,  True)   # zbocze narastające
            sleep(self.T_SCLK)
            GPIO.output(self.sclk,  False)  # zbocze opadające
            
            
    def recive_byte(self):
        '''
        Odbiera bajt do DS1302
        
        Returns:
            (Byte) transmisji
        '''
        GPIO.setup(self.io, GPIO.IN)    

        t_byte = 0
        i = 0
        while i <= 7:
            i+=1
                        
            sleep(self.T_SCLK)
            
            t_byte = t_byte >> 1
            
            if GPIO.input(self.io):
                t_byte |= 0x80 # 0b10000000   
            
            GPIO.output(self.sclk,  True)
            sleep(self.T_SCLK)
            GPIO.output(self.sclk,  False)
            
        return t_byte
    
    
    def write_single_byte(self, command_byte, data_byte):   
        '''
        Bajt polecenia z zapisem Bajtu do urzadzenia
        
        Args:
            command_byte: Byte komendy 
                @see: cmd_byte
            data_byte: Byte danych
        '''
        GPIO.output(self.ce, True)
        self.send_byte(command_byte)
        self.send_byte(data_byte)
        GPIO.output(self.ce, False)


    def read_single_byte(self, command_byte):
        '''
        Bajt polecenia z odczytem Bajtu z urzadzenia
        
        Args:
            command_byte: Byte komendy 
                @see: cmd_byte
        
        Returns:
            (Byte) danych
        '''
        GPIO.output(self.ce, True)
        self.send_byte(command_byte)
        rec_byte = self.recive_byte()
        GPIO.output(self.ce, False)
        return rec_byte
    
    
    def write_burst(self, command_burst, burst_data):
        '''
        Zapis Burst Write, całej sekwencji z poprzedzającą komendą
        
        Args:
            command_burst: Byte komendy, który musi zawierać adres
                który wskazuje na rejestr Burst
                @see: cmd_byte
            burst_data: lista Byte'ów danych
        '''
        GPIO.output(self.ce, True)
        
        self.send_byte(command_burst)
        
        for d_byte in burst_data: 
            self.send_byte(d_byte)
            
        GPIO.output(self.ce, False)


    def read_burst(self, command_burst, burst_len):
        '''
        Odczyt Burst Read, całej sekwencji z poprzedzającą komendą
        
        Args:
            command_burst: Byte komendy, który musi zawierać adres
                który wskazuje na rejestr Burst
                @see: cmd_byte
            burst_len: długość odczytu w Byte'ów po której przerwać transmisję
        
        Returns:
            (list[burst_len] of Bytes): odebrane Byte'y
        '''
        GPIO.output(self.ce, True)
        
        self.send_byte(command_burst)

        rec_data = []
        for i in range(0, burst_len): #@UnusedVariable
            rec_data.append(self.recive_byte())
        
        GPIO.output(self.ce, False)

        return rec_data
    