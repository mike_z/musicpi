#!/usr/bin/python
# -*- coding: utf-8 -*-


import smbus #@UnresolvedImport


#from hw import lcd_lib
#from hw import i2c_lib
from time import sleep
import RPi.GPIO as GPIO #@UnresolvedImport
get_bin = lambda x, n: x >= 0 and "0b"+str(bin(x))[2:].zfill(n) or "-"+"0b"+str(bin(x))[3:].zfill(n)



class AD7142:
    
    DEV_AD_ID                   = 0xE622
    
    # Registers 
    AD_SETUP                    = 0x000
    AD_PWR_CONTROL              = 0x000 # zasilane
    AD_STAGE_CAL_EN             = 0x001 # wł. kalibracji etapów
    
    AD_AMB_COMP_CTRL0           = 0x002
    AD_AMB_COMP_CTRL1           = 0x003
    AD_AMB_COMP_CTRL2           = 0x004
    
    AD_STAGE_LOW_INT_EN         = 0x005
    AD_STAGE_HIGH_INT_EN        = 0x006
    AD_STAGE_COMPLETE_INT_EN    = 0x007
    
    AD_STAGE_LOW_LIMIT_INT      = 0x008
    AD_STAGE_HIGH_LIMIT_INT     = 0x009
    AD_STAGE_COMPLETE_LIMIT_INT = 0x00A 
    
    AD_CDC_RESULT_S0            = 0x00B
    AD_CDC_RESULT_S1            = 0x00C
    AD_CDC_RESULT_S2            = 0x00D
    AD_CDC_RESULT_S3            = 0x00E
    AD_CDC_RESULT_S4            = 0x00F
    AD_CDC_RESULT_S5            = 0x010
    AD_CDC_RESULT_S6            = 0x011
    AD_CDC_RESULT_S7            = 0x012
    AD_CDC_RESULT_S8            = 0x013    
    AD_CDC_RESULT_S9            = 0x014
    AD_CDC_RESULT_S10           = 0x015
    AD_CDC_RESULT_S11           = 0x016
    
    AD_DEV_ID                   = 0x017
    
    
    AD_STAGE_CONFIG             = 0x080
    
    
    
    CIN_CON_NOCON       = 0b00 # niepołączony
    CIN_CON_NEGATIVE    = 0b01 # - 
    CIN_CON_POSITIVE    = 0b10 # +
    CIN_CON_BIAS        = 0b11 # nie używany w tym etapie
    

    
    def __init__(self, addr, bus_port):
        
        self.device_addr = addr
        self.bus = smbus.SMBus(bus_port)
    

    
    def sw_reset(self):
        '''
        Restartuje urządzenie, przywracając domyślne wartości we wszystkich 
        rejestrach. Podczas restartu przerywana jest komunikacja z urządzeniem,
        metoda wyłapuje wyjątek jako poprawne zrestartowanie urządzenia
        
        Return:
            True/False: wykrycie urządzenia
        '''
        try:
            self.write_word(0x0000, (1<<10))
        except Exception:
            print("Dev restart")
            
            
    def detect_ad7142(self):
        '''
        Detekcja obecności urządzenia AD7142
        
        Return:
            True/False: wykrycie urządzenia
        '''
        id_dev = 0
        try:
            id_dev = self.read_word(self.AD_DEV_ID)
        except Exception:
            print("Dev restart")
        
        return id_dev == self.DEV_AD_ID    
    
    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # -=-=- Rejestry konfiguracyjne -=-=- 
    def setup_pwr_control(self, low_power_mode = False, low_power_delay = 200, sequence_stages=12, 
                          adc_decimation_factor=256, interrupt_polarity_high=False, 
                          excitation_source_disable=False, src_inv_enable=False, cdc_bias=0):
        '''
        Konfiguracja rejestru zarządzania zasilaniem 
        0x000
        
        Args:
            low_power_mode: True = Tryb low power, wybudza co określony czas
                00 full power, 10 low power 
            low_power_delay: Opóźnienie wybudzenia w trybie low power (200, 400, 600, 800) 
                00 200ms, 01 400ms, 10 600ms, 11 800ms
            sequence_stages: Liczba etapów w sekwencji (1:12)
                0000 - 1 -> 1011 - 12 
            adc_decimation_factor: rozdzielczość wyników (128, 256)
                00 256bit, 01 128bit
            interrupt_polarity_high: True = polaryzacja przerwania dodatnia
            excitation_source_disable: True = wyłącza sygnał SRC
            src_inv_enable: True = włącza sygnał ^SRC
            cdc_bias: CDC Bias (0, 20, 35, 50)
                00 +0%, 01 +20%, 10 +35%, 11 +50%
        '''
        
        register = 0x0;
        
        # POWER_MODE
        if low_power_mode:
            register |= 0x2
        
        # LP_CONV_DELAY
        if low_power_delay >= 800:
            register |= 0xC
        elif low_power_delay >= 600:
            register |= 0x8
        elif low_power_delay >= 400:
            register |= 0x4
            
        # SEQUENCE_STAGE_NUM
        if sequence_stages > 12 | sequence_stages < 1:
            sequence_stages = 12
        
        register |= (sequence_stages - 1) << 4 & 0xF0 
        
        # DECIMATION
        if adc_decimation_factor == 128:
            register |= 0x0100
            
        # INT_POL
        if interrupt_polarity_high:
            register |= 0x0800
            
        # EXCITATION_SOURCE
        if excitation_source_disable:
            register |= 0x1000
            
        # ^SRC
        if not src_inv_enable:
            register |= 0x2000
            
        # CDC_BIAS
        if cdc_bias >= 50:
            register |= 0xC000
        elif cdc_bias >= 35:
            register |= 0x8000
        elif cdc_bias >= 20:
            register |= 0x4000
            
        #self.write_word(self.AD_PWR_CONTROL, register)
        return register
    
    def setup_stage_cal_en(self, stage0_cal_en = False, stage1_cal_en = False, stage2_cal_en = False, 
                           stage3_cal_en = False, stage4_cal_en = False, stage5_cal_en = False, 
                           stage6_cal_en = False, stage7_cal_en = False, stage8_cal_en = False,
                           stage9_cal_en = False, stage10_cal_en = False, stage11_cal_en = False,
                           avg_fullpower_skip = 3, avg_lowpower_skip = 0):
        '''
        Zwraca konfigurację rejestru zarządzania włączazniem kabliracji etapów
        0x001
        Args:
            stage0_cal_en: True - użyj kalibracji
            ...
            stage11_cal_en: j.w
            avg_fullpower_skip: średnia ilość ominiętych próbek w trybie pełnego zasilania (3, 7, 15, 31)
            avg_lowpower_skip: średnia ilość ominiętych próbek w trybie low power (0, 1, 2, 3)
        '''
        register = 0
        
        # STAGEx_CAL_EN
        if stage0_cal_en:
            register |= 1 << 0
        if stage1_cal_en:
            register |= 1 << 1
        if stage2_cal_en:
            register |= 1 << 2
        if stage3_cal_en:
            register |= 1 << 3
        if stage4_cal_en:
            register |= 1 << 4
        if stage5_cal_en:
            register |= 1 << 5
        if stage6_cal_en:
            register |= 1 << 6
        if stage7_cal_en:
            register |= 1 << 7
        if stage8_cal_en:
            register |= 1 << 8
        if stage9_cal_en:
            register |= 1 << 9
        if stage10_cal_en:
            register |= 1 << 10
        if stage11_cal_en:
            register |= 1 << 11
            
        # AVG_FP_SKIP
        if avg_fullpower_skip >= 31:
            register |= 0x3000
        elif avg_fullpower_skip >= 15:
            register |= 0x2000
        elif avg_fullpower_skip >= 7:
            register |= 0x1000   
            
        # AVG_LP_SKIP  
        if avg_lowpower_skip >= 3:
            register |= 0xC000
        elif avg_lowpower_skip >= 2:
            register |= 0x8000
        elif avg_lowpower_skip >= 1:
            register |= 0x4000       
        
        return register
    
    def setup_abm_comp_ctrl0(self, fast_filter_skip=0, fullpower_proximity_cnt=15, 
                             lowpower_proximity_cnt=15, powerdown_timeout=1.25, 
                             forced_calibration=False, conversion_reset=False):
        '''
        Zwraca konfigurację pierwszego rejestru kompensacji środowiskowej
        0x002
        Args:
            fast_filter_skip: ilość ominętych sekwencji na każdą w fast FIFO (0-11)
            fullpower_proximity_cnt: czas wyłączonej kalibracji = 
                fullpower_proximity_cnt x 16 x czas jednej sekwencji konwersji w trybie
                full power (zbliżanie do sensora wł., NONCONTACT PROXIMITY DETECTION)
            lowpower_proximity_cnt: czas wyłączonej kalibracji = 
                lowpower_proximity_cnt x 4 x czas jednej sekwencji konwersji w trybie
                low power
            powerdown_timeout: full to low power mode timeout 
                powerdown_timeout x fullpower_proximity_cnt
            forced_calibration: wymuszona kalibracja, True: wymusza wszystkie etapy konwersji 
                do rekalibracji gdy zajdzie
            conversion_reset: self-clearing
                true: resetuje kowersję do STAGE0
                
        '''        
        register = 0x0FF0
        
        # FF_SKIP_CNT
        if fast_filter_skip > 11 | fast_filter_skip < 0:
            fast_filter_skip = 0
        
        register |= (fast_filter_skip) & 0xF
        
        # FP_PROXIMITY_CNT
        if fullpower_proximity_cnt > 15 | fullpower_proximity_cnt < 0:
            fullpower_proximity_cnt = 15
        
        register |= (fullpower_proximity_cnt << 4) & 0xF0
        
        # LP_PROXIMITY_CNT
        if lowpower_proximity_cnt > 15 | lowpower_proximity_cnt < 0:
            lowpower_proximity_cnt = 15
        
        register |= (lowpower_proximity_cnt << 8) & 0xF00
        
        # PWR_DOWN_TIMEOUT
        if powerdown_timeout >= 2.00:
            register |= 0x3000
        elif powerdown_timeout >= 1.75:
            register |= 0x2000
        elif powerdown_timeout >= 1.50:
            register |= 0x1000  
        
        # FORCED_CAL
        if forced_calibration:
            register |= 4000
            
        # CONV_RESET 
        if conversion_reset:
            register |= 8000
            
        return register
    
    def setup_abm_comp_ctrl1(self, proximity_recalibration_level=0x64, 
                             proximity_detection_rate=0x1, slow_filter_update_level=0):
        '''
        
        0x003
        '''
        register = 0
        
        # PROXIMITY_RECAL_LVL 
        if proximity_recalibration_level > 0xFF | proximity_recalibration_level < 0:
            proximity_recalibration_level = 0x64
        
        register |= (proximity_recalibration_level) & 0xF
        
        # PROXIMITY_DETECTION_RATE 
        if proximity_detection_rate > 0x3F | proximity_detection_rate < 0:
            proximity_detection_rate = 0x1
        
        register |= (proximity_detection_rate << 8) & 0x3F00
        
        # SLOW_FILTER_UPDATE_LVL
        if slow_filter_update_level > 0x3 | slow_filter_update_level < 0:
            slow_filter_update_level = 0x0
        
        register |= (slow_filter_update_level << 14) & 0xC000
        
        return register
    
    def setup_abm_comp_ctrl2(self, fullpower_proximity_recall=0x3FF, lowpower_proximity_recall=0x3F):
        '''
        
        0x004
        '''
        register = 0
        
        # FP_PROXIMITY_RECAL
        if fullpower_proximity_recall > 0x3FF | fullpower_proximity_recall < 0:
            fullpower_proximity_recall = 0x3FF
        
        register |= (fullpower_proximity_recall) & 0x3FF
        
        
        # LP_PROXIMITY_RECAL
        if lowpower_proximity_recall > 0x3F | lowpower_proximity_recall < 0:
            lowpower_proximity_recall = 0x3F
        
        register |= (lowpower_proximity_recall << 10) & 0xFC00
        
        return register
    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # -=-=- STAGE Configuration -=-=- 
    
    def set_stage_inputs(self, stage_num, cin, afe_neg=False, afe_pos=False):
        '''
        Konfiguruje wejścia CINx
        
        Args:
            stage_num: numer etapu konwersji 0-13
            cin: lista
            afe_neg: negatywny offset afe
            afe_pos: pozytywny offset afe
        '''
        
        if stage_num > 11 | stage_num < 0:
            print("Numer stage poza zasięgiem 0 <> 11")
            return
        
        if len(cin) > 13 | len(cin) < 0:
            print("Zła ilość przekazanych konfiguracji portów, != 13")
            return
        
        # zebranie całej konfiguracji w jeden ciąg
        register = 0
        
        # bity konfiguracji są od najstarszego do najmłodszego
        # trzeba to poprawić w liście
        cin.reverse()
        
        for i in range(0, 14):
            register = (register << 2) | (cin[i] & 0x3) #0b000000xx  
        
        # konfiguracja dla CIN0-6 jest w innym rejestrze niż
        # dla CIN7-13, dodatkowo bity 15:14 są puste w pierszym 
        # rejestrze a w kolejnym dają możliwość sterowania AFE    
        # czyli offsetem +/- kondesatora 20pF "zasięgu" pomiaru
        # rys. 23 dokumentacja
        word_0 = register & 0x3FFF              # bez 2 bitów najwyższych 
        word_1 = (register >> 14) & 0x3FFF      # bez poprzednich 14 i 2 bitów najwyższych 
        
        # flagi afe offset
        if afe_neg:
            word_1 |= 0x4000
        if afe_pos:
            word_1 |= 0x8000
        
        # konfiguracje etapów STAGE są ułożone po 8 słów 
        # piersze 0x080 - CIN(6:0), 0x081 - CIN(13:7), następne 0x088         
        cfg_addr = self.AD_STAGE_CONFIG + (stage_num * 8)
        
        # zapis kolejnych 2 bloków
        self.write_block(cfg_addr, [word_0, word_1])
        
    
    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # -=-=- SMBus - i2c -=-=- 
    def write_word(self, register_16, data_16):
        '''
        Zapis do urządzenia słowa word (16 bits=2bytes) przy pomocy 
        adresacji word (16 bits=2bytes) 
        
        Args:
            register_16: ustawia wzkaźnik na wskazany adres (16bits=2Bytes)
            data_16: wartość zapisywana (16bits=2Bytes)
        '''
        
        # rozbicie adresu 16 bitowego na dwa 8 bitowe słowa
        # adresowanie tylko od 0x000 do 0x290
        reg_high = (register_16 >> 8) & 0x03 # 0b00000011 
        reg_low = register_16 & 0xFF
        
        # jw. z całymi bajtami
        data_high = (data_16 >> 8) & 0xFF
        data_low = data_16 & 0xFF
        
        # write dla smbus zawsze przyjmuje 8 bitowy adres więc druga połowa
        # adresu musi się znajdować w bloku, dla transmisji to obojętne
        self.bus.write_i2c_block_data(self.device_addr, reg_high, [reg_low, data_high, data_low]) 
        print("write_word at "+ hex(register_16) + " with " + hex(data_16))  
        
    def read_word(self, register_16):
        '''
        Ustawia wskaźnik na adres (16 bits=2bytes) i odczytuje słowo 
        word (16 bits=2bytes) 
        
        Args:
            register_16: ustawia wzkaźnik na wskazany adres (16bits=2Bytes)
        '''
        
        # rozbicie adresu 16 bitowego na dwa 8 bitowe słowa
        # adresowanie tylko od 0x000 do 0x290
        reg_high = (register_16 >> 8) & 0x03 # 0b00000011 
        reg_low = register_16 & 0xFF
        
        self.bus.write_byte_data(self.device_addr, reg_high, reg_low)
        
        result = self.bus.read_word_data(self.device_addr, 0x00) #dodatkowy parametr jest ignorowany 
    
        # byte-ty mają odwróconą kolejność, zamiana LLHH -> HHLL
        result_h = result & 0xFF
        result_l = (result & 0xFF00) >> 8
        return result_h << 8 | result_l 
    
    def write_block(self, register_16, data_16):
        '''
        Zapisuje cały blok informacji do urządzenia o adresie word (16 bits=2bytes)
        
        Args:
            register_16: ustawia wzkaźnik na wskazany adres (16bits=2Bytes)
            data: lista 8 bitowych 
        '''
        reg_high = (register_16 >> 8) & 0x03 # 0b00000011 
        reg_low = register_16 & 0xFF
        
        # lista ma wartości 16 bitowe a przesyłać przez i2c można 8 bitowe
        # trzeba je wszystkie rozbić
        
        data_8 = []
        for data_cur in data_16:
            data_8.append((data_cur >> 8) & 0xFF)   # H
            data_8.append(data_cur & 0xFF)          # L
        
        # argument może być tylko 8 bit więc trzeba wepchnąć drugą część 
        # adresu do bloku
        self.bus.write_i2c_block_data(self.device_addr, reg_high, [reg_low]+data_8)
        
    def read_block(self, register_16, data_len=16):
        
        if data_len > 16 | data_len < 1:
            data_len = 16
        
        reg_high = (register_16 >> 8) & 0x03 # 0b00000011 
        reg_low = register_16 & 0xFF
        
        self.bus.write_byte_data(self.device_addr, reg_high, reg_low)
        result = self.bus.read_i2c_block_data(self.device_addr, 0x00)
        
        data_16 = []
        # dane są ułożone po 8 bitów, trzeba je połaczyć w 16 bitów
        if len(result)%2 != 0:
            print("AD7142 i2c error: dane nie mają pełnych byte-ów")
        else:
            max_data_count = data_len
            
            if len(result) < data_len:
                max_data_count = len(result)
            
            for i in range(0, max_data_count, 2):
                word_data = result[i] << 8 # 0xHH00
                word_data = word_data | result[i+1] # 0xXXLL
                data_16.append(word_data)
        
        return data_16
    