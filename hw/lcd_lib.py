#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from hw import i2c_lib
from time import sleep #, time
import RPi.GPIO as GPIO #@UnresolvedImport

from logger import log


class lcd_hd44780:
	"""
	Biblioteka do obsługi wyświetlaczy HD44780 przez PCF8574
	Lista komend z dokkumentacji technicznej, fragmenty bibliotek 
	dostępnych dla raspbbery pi.
	
	data 	Raspberry Pi -> i2c -> 3.3V:5V -> PCF8574 -> linie P0-P7 -> HD44780
	E, RS 	Raspberry Pi -> gpio -> 3.3V:5V -> HD44780
	
	@todo: ujednolicenie strobe
	@todo: dodanie generowania własnych symboli
	@todo: obsługa cgram
	@todo: dodanie sygnaturek kodów symboli z generatora

	bazuje na: https://github.com/the-raspberry-pi-guy/lcd/blob/master/lcddriver.py
	"""
	
	# commands
	LCD_CLEARDISPLAY 	= 0x01
	LCD_RETURNHOME 		= 0x02
	LCD_ENTRYMODESET 	= 0x04  # zobacz flags for display entry mode
	LCD_DISPLAYCONTROL 	= 0x08  # zobacz flags for display on/off control
	LCD_CURSORSHIFT 	= 0x10  # zobacz flags for display/cursor shift
	LCD_FUNCTIONSET 	= 0x20  # zobacz flags for function set
	LCD_SETCGRAMADDR 	= 0x40
	LCD_SETDDRAMADDR 	= 0x80
	
	# flags for display entry mode
	LCD_ENTRYRIGHT 			= 0x00
	LCD_ENTRYLEFT 			= 0x02
	LCD_ENTRYSHIFTINCREMENT = 0x01
	LCD_ENTRYSHIFTDECREMENT = 0x00

	# flags for display on/off control
	LCD_DISPLAYON 	= 0x04
	LCD_DISPLAYOFF 	= 0x00
	LCD_CURSORON 	= 0x02
	LCD_CURSOROFF 	= 0x00
	LCD_BLINKON 	= 0x01
	LCD_BLINKOFF 	= 0x00
	
	# flags for display/cursor shift
	LCD_DISPLAYMOVE = 0x08
	LCD_CURSORMOVE 	= 0x00
	LCD_MOVERIGHT 	= 0x04
	LCD_MOVELEFT 	= 0x00
	
	# flags for function set
	LCD_8BITMODE 	= 0x10
	LCD_4BITMODE 	= 0x00
	LCD_2LINE 		= 0x08
	LCD_1LINE 		= 0x00
	LCD_5x10DOTS 	= 0x04
	LCD_5x8DOTS 	= 0x00
	
	# flags of line address
	LCD_LINE1 		= 0x00
	LCD_LINE2 		= 0x40
	
	
	def __init__(self, pcf_8574_addr, lcd_pin_e, lcd_pin_rs):
		"""
		Inicjalizacja wyświetlacza
		
		Args:
			pcf_8574_addr: adres urządzenia na magistrali i2c
			pin_e: bit podłączenia pinu enable (strob)
			pin_rw: bit kierunku (0 zapisu/1 odczytu)
			pin_rs: wybieranie rejestru pamięci
		"""
		
		log.debug('init lcd device addr @ 0x{0:02x}, E @ {1}, RS @ {2} '.format(pcf_8574_addr, lcd_pin_e, lcd_pin_rs))
		
		GPIO.setmode(GPIO.BOARD) 	# numery jak piny w złączu 
		GPIO.setwarnings(False) 	# ukrywa "already in use"
		
		# adres PCF 8574
		self.lcd_device = i2c_lib.i2c_device(pcf_8574_addr)
		
		# piny lcd	
		self.pin_e		= lcd_pin_e		
		self.pin_rs		= lcd_pin_rs
		GPIO.setup(self.pin_e, GPIO.OUT)	# jako wyjście
		GPIO.setup(self.pin_rs, GPIO.OUT)
		GPIO.output(self.pin_e, False)		# stan niski (LOW: 0)
		GPIO.output(self.pin_rs, False)
		self.init_hd44780()
		
		
	def init_hd44780(self):	
		'''	
		Sekwencja zerowania, pozwala na restart wyświetlacza 
		nawet uruchmionego wbrew temu co mówi instrukcja ;)
		'''
		log.debug('lcd init sequence')
		self.lcd_write(0x03) 
		sleep(0.2)
		self.lcd_write(0x03)
		sleep(0.2)
		self.lcd_write(0x03)
		sleep(0.2)
		#self.lcd_write(0x02) #dla 4 bitmode
		self.lcd_write(0x38)
		sleep(0.2)
		
		
		# procedura z manuala HD44780
		sleep(0.4)
		self.lcd_write(self.LCD_FUNCTIONSET | self.LCD_2LINE | self.LCD_5x8DOTS | self.LCD_8BITMODE)
		self.lcd_write(self.LCD_DISPLAYCONTROL | self.LCD_DISPLAYON)
		self.lcd_write(self.LCD_CLEARDISPLAY)
		self.lcd_write(self.LCD_ENTRYMODESET | self.LCD_ENTRYLEFT)
		sleep(0.4)

		
	def lcd_write(self, cmd, register_select=False):
		"""
		Wysłanie 8 bitów do urządzenia
		
		Args:
			cmd: bity do wysłania
			register_select: 
		"""
		
		if register_select != False:	# gdy adresowanie rejestrów pamięci	
			GPIO.output(self.pin_rs, True)
		
		self.lcd_device.write_cmd(cmd)	# zapis Bajtu
		sleep(.001)
		
		GPIO.output(self.pin_e, True) 	# strobe za pomocą osobnego pinu
		sleep(.001)  
		
		GPIO.output(self.pin_e, False)	# czyszczenie pinu E
		sleep(.001)  

		GPIO.output(self.pin_rs, False)	# czyszczenie RS
		
		
	def lcd_clear(self):
		"""
		Czyszczenie ekranu z zawartości oraz ustawienie w pozycji home
		
		!unikać tego sposobu czyszczenia za wszelką cenę!
		Trwa dłużej niż zapisanie całego bufora ekranu na nowo
		"""
		
		self.lcd_write(self.LCD_CLEARDISPLAY)
		self.lcd_write(self.LCD_RETURNHOME)
		
		
	def lcd_display_string(self, string, line=0, pos=0):
		"""
		Wysyła cały string na ekran 
		
		Args:
			string: ciąg do wysłania (uwaga na UTF, ASCII !!!)
			line: linia tekstu na wyświetlaczu (1 lub 2)
			pos: pozycja liczona od 0 
		"""
		
		# korygowanie pozycji ze względu na linię
		if line == 0:
			pos = self.LCD_LINE1 + pos
		elif line == 1:
			pos = self.LCD_LINE2 + pos

		# zapis pozycji
		self.lcd_write(self.LCD_SETDDRAMADDR | pos)

		# poprawnie polskich czcionek
		string = self.lcd_fix_polish_char(string)
		
		# zapis z flagą RS
		for char in string:
			self.lcd_write(ord(char), True)	
	
	def lcd_set_cgram(self, address, data):
		
		if len(data) != 8:
			print("lcd_set_cgram ! CGRAM data != 8 byte!!!")
		
		print("lcd_write " + get_bin(self.LCD_SETCGRAMADDR | address, 8) )
		
		self.lcd_write(self.LCD_SETCGRAMADDR | address)
		sleep(0.05) #0.038
		for i in range(0, 8):
			print(str(i) + "in range set " + get_bin(data[i] & 0b00011111, 8) )
			self.lcd_write(data[i] & 0b00011111, 1)
			
			i+=1
			
			
	def lcd_fix_polish_char(self, string):
		"""
		Naprawienie polskich znaków z czcionek dostępnych w wyświetlaczu 
		z kas UPOS (przesunięte czcionki względem standardu ASCII)
		
		Args:
			string: ciąg do poprawy znaków
		
		Returns:
			string: ciąg z zmienionymi znakami prawidłowo wyświetlanymi
		"""
		string = string.replace("ą", chr(0x9F))
		string = string.replace("Ą", chr(0x9F))
		string = string.replace("ć", chr(0xAD))
		string = string.replace("Ć", chr(0xAD))
		string = string.replace("ę", chr(0xAF))
		string = string.replace("Ę", chr(0xAF))
		string = string.replace("ł", chr(0xA0))
		string = string.replace("Ł", chr(0xA0))
		string = string.replace("ń", chr(0xB3))
		string = string.replace("Ń", chr(0xB3))
		string = string.replace("ó", chr(0x93))
		string = string.replace("Ó", chr(0x93))
		string = string.replace("ś", chr(0xA2))
		string = string.replace("Ś", chr(0xA2))
		string = string.replace("ź", chr(0xA7))
		string = string.replace("Ź", chr(0xA7))
		string = string.replace("ż", chr(0xA9))
		string = string.replace("Ż", chr(0xA9))	
		return string	
	
			

get_bin = lambda x, n: x >= 0 and "0b"+str(bin(x))[2:].zfill(n) or "-"+"0b"+str(bin(x))[3:].zfill(n)
