#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

import RPi.GPIO as GPIO #@UnresolvedImport

from hw import i2c_lib

from logger import log

class Keybrd_PCF8574:
    
    
    def __init__(self, pcf_8574_addr, int_io):
               
        log.debug('init keybrd')       
        
        self.keybrd_device = i2c_lib.i2c_device(pcf_8574_addr)
        
        self.int_io = int_io
        
        GPIO.setmode(GPIO.BOARD)    # numery jak piny w złączu 
        GPIO.setwarnings(False)     # ukrywa "already in use"
        
        GPIO.setup(self.int_io, GPIO.IN)    # jako wejście
        
        # bufor odczytu
        self.prev_status = []
        for i in range(0,8): #@UnusedVariable
            self.prev_status.append(False)
        
    def __del__(self):
        pass
        
        
    def get_status(self):
        '''
        Zwraca informacje o przerwaniu
        
        Returns:
            True - gdy złoszono flagę przerwania a przyciski mają 
            zmienioną pozycję (stan)
        '''
        
        if not GPIO.input(self.int_io):
            return True
        else:
            return False
        
        

    def read_io(self):
        '''
        Odczytuje i zwraca stan portów io układu PCD8574
        Odwraca znaczenie bitów, 1 przycisk puszony (pull-up) = False
        0 przcisk naciśniety = True
        
        Returns:
            Lista len() = 8, True/False
            np. [True, False, False, False, True, False, False, False]
            dla key 0 oraz 4 naciśniętych
        '''
        
        output_list = []    # lista True/False dla stanów IO
        
        port_data = self.keybrd_device.read() # odczyt z pcf
        
        # dekodowanie z bitów na listę 
        for i in range(0, 8):
            # 1 - pullup, NIE naciśniety
            if (port_data >> i) & 0x1 == 0x1:
                output_list.append(False)
            # 0 - naciśnięty
            else:
                output_list.append(True)
        
        return output_list
    
            
        