#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from hw import i2c_lib
from logger import log

class PWM_PCA9633:
	'''
	Sterownik pwm dla układu PCA9633
	
	@todo: dodać możliwość migania diodami, lub pulsowania
	@todo: ustawianie selektywne jednego z wyjść
	@todo: konfiguracja słowna według dokumentacji
	'''
	
	# commands
	REG_MODE1 	= 0x00 # mode register
	REG_MODE2	= 0x01
	REG_PWM0	= 0x02 # jasność
	REG_PWM1	= 0x03
	REG_PWM2	= 0x04
	REG_PWM3	= 0x05
	REG_GRPPWM	= 0x06 # sterowanie grupą duty cycle
	REG_GRPFREQ	= 0x07 # grupa frequency
	REG_LEDOUT	= 0x08 # led output state
	REG_SUBADR1	= 0x09
	REG_SUBADR2	= 0x0A
	REG_SUBADR3	= 0x0B
	REG_ALLCALL	= 0x0C
	
	# maski auto increment 
	MASK_AUTOINC_ALL = 0b10000000 # All register auto increment
	MASK_AUTOINC_LED = 0b10100000 # only led invd control (LED0-3)
	MASK_AUTOINC_GLB = 0b11000000 # global control reg
	MASK_AUTOINC_LEDG = 0b11100000 # global and led indv

	
	def __init__(self, addr_pca9633, LED0, LED1, LED2, LED3):
		
		self.addr_pca9633 = addr_pca9633
		self.LED0 = LED0
		self.LED1 = LED1
		self.LED2 = LED2
		self.LED3 = LED3
		
		# adres 
		self.pwm_device = i2c_lib.i2c_device(self.addr_pca9633)
				
		self.pwm_device.write_i2c_block_data(0x80, 
											[0x00, # MODE 1 no extra address
											0x15, # MODE 2 ext drv, high-imped @ 0
											self.LED0, # LED 0 to 3 PWMx
											self.LED1, 
											self.LED2, 
											self.LED3, 
											0xff, # group duty cycle 100%
											0x00, # group freq
											0xaa, # LEDOUT for indv. LED PWMx
											0xe2, # sub addresses - not used
											0xe4, 
											0xe8])
		#self.update_pwm()
		
	def __del__(self):
		log.debug("__del__ pwm_PCA9633")
		pass
	
	
	def update_pwm(self, LED0, LED1, LED2, LED3):
		self.LED0 = LED0
		self.LED1 = LED1
		self.LED2 = LED2
		self.LED3 = LED3
		'''
		Aktualizuje stan sterowania PWM
		(wartości przypisane przez zmienne obiektu lub metodą set
		'''
		
		# REG_PWM0 | MASK_AUTOINC_LED
		self.pwm_device.write_i2c_block_data(0xA2, [LED0, LED1, LED2, LED3])
