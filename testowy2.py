#!/usr/bin/python
# -*- coding: utf-8 -*-

from hw import lcd_lib
from hw import i2c_lib

from hw import ad7142_lib
from hw import ds1302_lib
from hw import encoder_lib

from time import sleep, strftime, time
from datetime import datetime
import RPi.GPIO as GPIO #@UnresolvedImport
#from __builtin__ import str



get_bin = lambda x, n: x >= 0 and "0b"+str(bin(x))[2:].zfill(n) or "-"+"0b"+str(bin(x))[3:].zfill(n)


#enc = encoder_lib.HwEncoder(29, 31)

#counterr = 0
#while 1:
    #readout = enc.read_encoder()
    #counterr += readout
#    print("Encoder: " + str(enc.position))
#    sleep(0.01)


    
#13 12 16
#ds = ds1302_lib.DS1302(pin_io=33, pin_sclk=32, pin_ce=36)

#print(ds.get_datetime())

#ds.set_datetime(datetime.now())

#print(datetime.now())


#ds_time = ds.read_single_byte(ds.set_command_byte(write_enable=True, ram_access=False, 
#                                                  address_half_byte=0, write=False))

#ds.write_single_byte(0x80, 0)

#byte_list = [0x81, 0x83, 0x85, 0x87, 0x89, 0x8B, 0x8D]

#for com in byte_list:
#    ds_data = ds.read_single_byte(com) 
#    print(hex(com) + " : " + get_bin(ds_data, 8))

#ds.clock_writeprotect(False)
#ds.clock_halt(False) 

#ds.write_single_byte(0x80, 0x80)
#ds.write_single_byte(0xbf, 0)

#for com in byte_list:
#    ds_data = ds.read_single_byte(com) 
#   print(hex(com) + " : " + get_bin(ds_data, 8))  


#burst_data = ds.read_burst(ds.cmd_byte(half_byte=ds.HB_BURST, write=False), 3)

#i = 0
#for com in burst_data:
#    print(hex(i) + " : " + get_bin(com, 8))  
#    i+=1

#ds_time = ds.read_single_byte(0x81)
#ds_time = ds.read_single_byte(0x8D)

#print(hex(get_bin(ds_time, 16))

#while 1:
#rtc_datetime = ds.get_datetime()

#print("Time rtc: " + str(rtc_datetime["hour"]) + ":" + str(rtc_datetime["minute"]) 
#      + ":" + str(rtc_datetime["second"]))

#print("Date rtc: " + str(rtc_datetime["year"]) + "-" + str(rtc_datetime["month"])
#      + "-" + str(rtc_datetime["date"]) + "," + str(rtc_datetime["day"]))

#sleep(1)





ad = ad7142_lib.AD7142(0x2C, 1)

ad.sw_reset()


set_setup = ad.setup_pwr_control(low_power_mode = False, 
                                 low_power_delay = 200, 
                                 sequence_stages = 4, 
                                 adc_decimation_factor = False, 
                                 interrupt_polarity_high = False, 
                                 excitation_source_disable = False, 
                                 src_inv_enable = False, 
                                 cdc_bias = 0
                                 ) #0x00B0

set_stage = 0x0000

set_abm0 = ad.setup_abm_comp_ctrl0(fast_filter_skip = 0, 
                                   fullpower_proximity_cnt = 1, 
                                   lowpower_proximity_cnt = 8, 
                                   powerdown_timeout = 1.25, 
                                   forced_calibration = True, 
                                   conversion_reset = True
                                   )

set_abm1 = ad.setup_abm_comp_ctrl1(proximity_recalibration_level = 0x70, 
                                   proximity_detection_rate = 0x60, 
                                   slow_filter_update_level = 0x02
                                   )

set_abm2 = ad.setup_abm_comp_ctrl2(fullpower_proximity_recall = 0x16, 
                                   lowpower_proximity_recall = 0x03
                                   )

set_clr = 0x0
        
ad.write_block(ad.AD_SETUP, [set_setup, set_stage, set_abm0, set_abm1, set_abm2, set_clr, set_clr, set_clr])


ad.set_stage_inputs(0, [ad.CIN_CON_NEGATIVE, ad.CIN_CON_POSITIVE, ad.CIN_CON_BIAS, 
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,], afe_neg=False, afe_pos=False)

ad.set_stage_inputs(1, [ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, ad.CIN_CON_NEGATIVE, 
                        ad.CIN_CON_POSITIVE, ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,], afe_neg=False, afe_pos=False)


ad.set_stage_inputs(2, [ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, 
                        ad.CIN_CON_BIAS, ad.CIN_CON_NEGATIVE, ad.CIN_CON_POSITIVE,
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,], afe_neg=False, afe_pos=False)

ad.set_stage_inputs(3, [ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, 
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,
                        ad.CIN_CON_NEGATIVE, ad.CIN_CON_POSITIVE, ad.CIN_CON_BIAS,
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,
                        ad.CIN_CON_BIAS, ad.CIN_CON_BIAS,], afe_neg=False, afe_pos=False)







set_stage = ad.setup_stage_cal_en(True, True, True, True, False, False,
                                  False, False, False, False, False, False, 3, 0) #0x0000

ad.write_word(ad.AD_STAGE_CAL_EN, set_stage)


'''


ad.write_word(0x000, 0x00B2)
ad.write_word(0x001, 0x0000)
ad.write_word(0x002, 0x3230)
ad.write_word(0x003, 0x14C8)
ad.write_word(0x004, 0x0832)
ad.write_word(0x005, 0x0000)
ad.write_word(0x006, 0x0000)
ad.write_word(0x007, 0x0001)

ad.write_word(0x001, 0x0FFF)

'''
stage_add = 0x0B
counter = 0

while (1):
    
    stage_num = 0
    print(ad.detect_ad7142())
    
    for stage_num in range(0, 12):
        result = ad.read_word(stage_add + stage_num)
        print("stage" + str(stage_num) + "\t: " +  str(result)) # get_bin(result, 16) + " : " +
        
    print("-------------"+str(counter)+"---------------")
    counter += 1
    sleep(0.1)
    

'''

ad.write_word(0x000, 0x00B0)

res = ad.read_word(0x000)
print("read : " + get_bin(res, 16) + " : " +  hex(res))
sleep(2)

#ad.write_word(0x000, 0x04B0)
ad.sw_reset()

sleep(1)
res = ad.read_word(0x017)
print("read : " + get_bin(res, 16) + " : " +  hex(res))
'''
'''
ad.write_i2c_block_data(0x00, [0x00, 0x00, 0xB0]) +
 
ad.write_i2c_block_data(0x00, [0x01, 0x00, 0x00]) +
ad.write_i2c_block_data(0x00, [0x02, 0x32, 0x30]) 0
ad.write_i2c_block_data(0x00, [0x03, 0x14, 0xC8]) +
ad.write_i2c_block_data(0x00, [0x04, 0x08, 0x32]) +
ad.write_i2c_block_data(0x00, [0x05, 0x00, 0x00]) +
ad.write_i2c_block_data(0x00, [0x06, 0x00, 0x00]) +
ad.write_i2c_block_data(0x00, [0x07, 0x00, 0x01])0

ad.write_i2c_block_data(0x00, [0x01, 0x0F, 0xFF]) +

while (1):
    #ad.write_cmd_arg(0x00, 0x00)
    #result0 = ad.read_word_data(0x00)
    
    ad.write_cmd_arg(0x00, 0x08)
    result1 = ad.read_i2c_block_data(0x02)

    print(str(len(result1)))
    
    #print("H : " + get_bin(result0 & 0b11111111, 8))
    #print("L : " + get_bin((result0 >> 8) & 0b11111111, 8))
    #print("H : " + get_bin(result1, 16) + " : " + str(result1))
    #print("L : " + get_bin(result2, 16) + " : " + str(result2))
    

    for i in range(0, len(result1), 2):
        print(hex((i/2)+0x08) + " : " + get_bin(result1[i], 8) + get_bin(result1[i+1], 8))
    
    sleep(0.25)


'''


'''

ad = i2c_lib.i2c_device(0x2c, 1)
#ad = i2c_lib.i2c_device(0x38, 1)

#ad.write_block_data(0x00, [0x00, 0x00, 0xB0])
ad.write_i2c_block_data(0x00, [0x00, 0x00, 0xB0])

ad.write_i2c_block_data(0x00, [0x01, 0x00, 0x00])
ad.write_i2c_block_data(0x00, [0x02, 0x32, 0x30])
ad.write_i2c_block_data(0x00, [0x03, 0x14, 0xC8])
ad.write_i2c_block_data(0x00, [0x04, 0x08, 0x32])
ad.write_i2c_block_data(0x00, [0x05, 0x00, 0x00])
ad.write_i2c_block_data(0x00, [0x06, 0x00, 0x00])
ad.write_i2c_block_data(0x00, [0x07, 0x00, 0x01])

ad.write_i2c_block_data(0x00, [0x01, 0x0F, 0xFF])

while (1):
    #ad.write_cmd_arg(0x00, 0x00)
    #result0 = ad.read_word_data(0x00)
    
    ad.write_cmd_arg(0x00, 0x08)
    result1 = ad.read_i2c_block_data(0x02)

    print(str(len(result1)))
    
    #print("H : " + get_bin(result0 & 0b11111111, 8))
    #print("L : " + get_bin((result0 >> 8) & 0b11111111, 8))
    #print("H : " + get_bin(result1, 16) + " : " + str(result1))
    #print("L : " + get_bin(result2, 16) + " : " + str(result2))
    

    for i in range(0, len(result1), 2):
        print(hex((i/2)+0x08) + " : " + get_bin(result1[i], 8) + get_bin(result1[i+1], 8))
    
    sleep(0.25)
'''

'''
while (1):
    for i in range(0x0170, 0x0180):
        result = ad.read_word_data(i)
        print(hex(i) + " : " + get_bin(result, 16) + " : " + str(result))
    sleep(0.2)
'''
'''
addr1 = 0x008
addr2 = 0x009
addr3 = 0x00A


while (1):
    result1 = ad.read_word_data(addr1)
    result2 = ad.read_word_data(addr2)
    result3 = ad.read_word_data(addr3)
    print(hex(addr1, ) + " : " + get_bin(result1, 16) + " : " + str(result1))
    print(hex(addr2, ) + " : " + get_bin(result2, 16) + " : " + str(result2))
    print(hex(addr3, ) + " : " + get_bin(result3, 16) + " : " + str(result3))
    sleep(1)
'''
'''
while (1):
    result = ad.read()
    print(get_bin(result, 8) + " : " + str(result))
    sleep(0.5)
'''