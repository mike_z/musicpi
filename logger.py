#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

'''
Prosty wraper dla loggera

wykorzystanie:

 from logger import log

w efekcie otrzymany zostanie głowny logger dla calej aplikacji

@see: main.py
'''

import logging

log = logging.getLogger('root')