#!/usr/bin/python
# -*- coding: utf-8 -*-

from hw import lcd_lib
from time import sleep, strftime, time
#from __builtin__ import str
from hw.pwm_lib import pwm_PCA9633


#pwm = pwm_PCA9633(0x62)

#pwm.set(10, 30, 0, 0)

from hw import i2c_lib

get_bin = lambda x, n: x >= 0 and "0b"+str(bin(x))[2:].zfill(n) or "-"+"0b"+str(bin(x))[3:].zfill(n)

pwm = i2c_lib.i2c_device(0x62)


for i in range(0,12):
    var = pwm.read_data(i)
    print( hex(i) + " " + hex(var) + " " + get_bin(var, 8))
    pass

pwm.write_block_data(0x80, [0x00, 0x15, 0x00, 0x00, 
                            0x00, 0x00, 0xff, 0x00, 
                            0xaa, 0xe2, 0xe4, 0xe8])

for i in range(0,12):
    var = pwm.read_data(i)
    print( hex(i) + " " + hex(var) + " " + get_bin(var, 8))
    pass

pwm.write_block_data(0xA2, [25, 50, 75, 100])


'''
pwm.read()
print("write pwm")
pwm.write_cmd_arg(0x00, 0x00)
sleep(0.1)
pwm.write_cmd_arg(0x01, 0x15)
sleep(0.1)
pwm.write_cmd_arg(0x08, 0xAA)
sleep(0.1)

print("write pwm")
pwm.write_cmd_arg(0x02, 50)
pwm.write_cmd_arg(0x03, 63)
pwm.write_cmd_arg(0x04, 63)
pwm.write_cmd_arg(0x05, 63)
'''
'''
pass
ttime = 0.1

print("set0")
for i in range(0,256,5):
    pwm.write_cmd_arg(0x02, i)
    print("-> " + str(i))
    sleep(ttime)

print("set1")
for i in range(0,256,5):
    pwm.write_cmd_arg(0x03, i)
    print("-> " + str(i))
    sleep(ttime)
    
print("set2")
for i in range(0,256,5):
    pwm.write_cmd_arg(0x04, i)
    print("-> " + str(i))
    sleep(ttime)
    
print("set3")
for i in range(0,256,5):
    pwm.write_cmd_arg(0x05, i)
    print("-> " + str(i))
    sleep(ttime)   


#sleep(0.5)

for i in range(0,12):
    print( hex(i) + " " + get_bin(pwm.read_data(i), 8))
    pass
'''
'''
lcd1 = lcd_lib.lcd_hd44780(0x38, 16, 22)
lcd2 = lcd_lib.lcd_hd44780(0x38, 18, 22)

lcd1.lcd_clear()
lcd2.lcd_clear()
                    #   12345678901234567890
#lcd1.lcd_display_string("12345678901234567890", 1)
lcd1.lcd_display_string("       musicPi v1.0 ", 0)
lcd1.lcd_display_string(" Michał Żak         ", 1)
lcd2.lcd_display_string("  *  .  *   .     . ", 0)
lcd2.lcd_display_string(" .  *  *  .   +     ", 1)

sleep(0.5)

lcd1.lcd_clear()   
lcd2.lcd_clear()


#lcd1.lcd_display_string(chr(0x00), 0)
##print("write char")
#sleep(2)
#print("write to cgram")
#lcd1.lcd_set_cgram(0x00, [0, 0xa, 0, 0x4, 0x0, 0x11, 0xe, 0])
#print("again char")
#lcd1.lcd_display_string(chr(0x00)+ " vs " + chr(0x08) + " " + chr(0xFF), 0)
'''

'''
is_started = 1
start_time = 0
timed = 0
time_formated = strftime('%H:%M:%S')


import RPi.GPIO as GPIO #@UnresolvedImport # always needed with RPi.GPIO  
  
GPIO.setmode(GPIO.BOARD)  # choose BCM or BOARD numbering schemes. I use BCM  
  
GPIO.setup(11, GPIO.OUT)# set GPIO 25 as an output. You can use any GPIO port  
  
p = GPIO.PWM(11, 25000)    # create an object p for PWM on port 25 at 50 Hertz  
                        # you can have more than one of these, but they need  
                        # different names for each port   
                        # e.g. p1, p2, motor, servo1 etc.  
  
p.start(1)             # start the PWM on 50 percent duty cycle  
                        # duty cycle value can be 0.0 to 100.0%, floats are OK  
  
#p.ChangeDutyCycle(90)   # change the duty cycle to 90%  
  
#p.ChangeFrequency(100)  # change the frequency to 100 Hz (floats also work)  
                        # e.g. 100.5, 5.2  
  
#p.stop()                # stop the PWM output  
  
#GPIO.cleanup()          # when your program exits, tidy up after yourself  

a = 5
p.ChangeDutyCycle(a)
lcd1.lcd_display_string("Ch.DutyCycle("+str(a)+")", 0)
b = 110
lcd2.lcd_display_string("Ch.Frequency("+str(b)+")", 0)
p.ChangeFrequency(b)  # change the frequency to 100 Hz (floats also work)  
i = 0
is_started = 1
while is_started == 1:
    
    lcd1.lcd_display_string("Ch.DutyCycle("+str(i)+")", 0)
    p.ChangeDutyCycle(i)  # change the frequency to 100 Hz (floats also work)  
                         # e.g. 100.5, 5.2  
    if i == 100:
        i = 0
    else:
        i+=1
    timed = round(time() - start_time, 4)
    start_time = time()
    
    time_formated = strftime('%H:%M:%S')
    #                        12345678901234567890
    lcd1.lcd_display_string("     "+time_formated+"     "+chr(0x00), 0)
    lcd1.lcd_display_string("    "+strftime('%Y-%m-%d')+"   ", 1)
    lcd2.lcd_display_string("                    ", 0)
    lcd2.lcd_display_string(str(timed)+"              ", 1)
    
    sleep(0.5)

p.stop()                # stop the PWM output  
  
GPIO.cleanup()          # when your program exits, tidy up after yourself  
'''
"""
    timed = round(time() - start_time, 4)
    start_time = time()
    
    time_formated = strftime('%H:%M:%S')
    #                        12345678901234567890
    lcd1.lcd_display_string("      "+time_formated+"      ", 1)
    lcd1.lcd_display_string("   "+strftime('%Y-%m-%d %a')+"    ", 2)
    lcd2.lcd_display_string("                    ", 1)
    lcd2.lcd_display_string("cycle: "+str(timed)+"       ", 2)
    #sleep(0.4)
"""

"""
#sleep(3)



lcd1.lcd_clear()

i = 0
#endi = i+4

lcd1.lcd_display_string("Press enter", 0)

while i < 255:
    
    try:
        input("Press enter to continue " + str(i) + " - " + str(i+4))
    except SyntaxError:
        pass
    
    endi = i+4
    line = 0
    text = ["", ""];
    
    while i < endi:
        
        if line >= 2:
            break
            
        if len(text[line]) + 3 < 20:
            text[line] = text[line] + str(i) + ":" + chr(i) + " " 
            i+=1
        else:
            line+=1
    
    lcd1.lcd_display_string(text[0], 0)
    lcd1.lcd_display_string(text[1], 1)
    
    
    

'''
    text = ""
    
    print("from " + str(i) + " to " + str(endi))
    
    for j in range(i, i+20):
        if j > 254:
            break
        text = text + str(j) + ":" + chr(j) + " "
    
    i = i + 20
    
    lcd1.lcd_display_string(text, line); 
    
    line += 1;
    
    if line > 1:
        line = 0     

    sleep(0)'''
"""  
'''
lcd1.lcd_display_string(chr(0x00), 0)
print("write char")
sleep(2)
print("write to cgram")
lcd1.lcd_set_cgram(0x00, [0, 0xa, 0, 0x4, 0x0, 0x11, 0xe, 0])
print("again char")
lcd1.lcd_display_string(chr(0x00)+ " vs " + chr(0x08) + " " + chr(0xFF), 0)
'''