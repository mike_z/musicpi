#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

import os

class PiOS:
    
    @staticmethod
    def getNetworkState():
        res = os.popen("cat /sys/class/net/eth0/carrier").read()
        if res[0:1] == '1':
            return True
        else:
            return False
        
    @staticmethod
    def shutdown():
        print '------------Shutdown-------------'
        pass
        
    