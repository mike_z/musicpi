#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from logger import log

class MP_Data:
        
    _instance = None
    
    
        
    
    
    def __init__(self):
        log.debug('Nowy obiekt MP_Data')
        
    @staticmethod
    def Instance():
        '''
        Implementacja singletonu
        '''
        if MP_Data._instance == None:
            MP_Data._instance = MP_Data()
        
        log.debug('get Instance MP_Data')
        return MP_Data._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    # Fjuczer :D
    
    
    def load_conf(self, file_path):
        '''
        Otwiera plik konfiguracji do odczytu i przeksztacenia na zmienn
        '''
        f = open(file_path, 'r')
        
        f.close()
        pass
    
    def save_conf(self, file_path):
        '''
        Zapisuje bierzącą konfigurację znajdującą w się w instancji live_configuration
        '''
        f = open(file_path, 'w')
        
        f.close()
        pass