#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from music_pi.mp_hook import MP_hook
from music_pi.mp_msg import MP_msg

from logger import log
import threading
from time import sleep

class MP_Events:
    
    mp_messages = []    # lista wiadomości  
    mp_hooks = []       # lista uchwytów
    
    _instance = None
    
    mp_loop_timer = 0.25 # czas odświeżenia pętli
    
    def __init__(self):
        log.debug('Nowa lista fifo')
        self._init_loop();
        
    @staticmethod
    def Instance():
        '''
        Zwraca zawsze jeden obiekt MP_Events przestawiajacy całą obsługę 
        komunikatów systemowych
        '''
        if MP_Events._instance == None:
            MP_Events._instance = MP_Events()
        
        #log.debug('get Instance MP_Events')
        return MP_Events._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)
    
    def __del__(self):
        self._th_valid = False
        sleep(self._th_time)
    
    #-----------------------
    
    def _init_loop(self):
        self._th_valid = True
        self._th_time = self.mp_loop_timer
        self._th_thread = threading.Thread(target=self._event_loop, name='EV_LOOP')
        self._th_thread.daemon = True # zakończy się razem z głównym wątkiem
        self._th_thread.start()   
    
    def _event_loop(self):
        while self._th_valid:
            
            if len(self.mp_messages) > 0: 
                temp_messages = self.mp_messages[:] # copy no ref
                self.mp_messages = []
                
                
                for i in range(0, len(temp_messages)): #@UnusedVariable
                    item = temp_messages.pop(0) #reverse
                                   
                    for hook in self.mp_hooks:
                        if hook.event == item.event: 
                            #try:        
                                hook.ref(item.source, item.data)
                            #except(TypeError):
                            #    try:
                            #        hook.ref()
                            #    except:
                            #        log.error('hook error 0 arg, {0}'.format(hook.name))
                            #except:
                            #    log.error('hook error, {0}'.format(hook.name))
            
            sleep(self._th_time)
            

    def new_event(self, source, event, data):
        '''
        Nowe wydarzenie dodawane do listy wiadomości
        '''
        self.mp_messages.append(MP_msg(source, event, data))
        log.debug('new event {0}'.format(event))
        
    
    def add_hook(self, event, name, ref):   
        '''
        Nowy uchwyt obsługi wydarzenia
        ''' 
        self.mp_hooks.append(MP_hook(event, name, ref))
        log.debug('new hook {0}'.format(name))
        
        
    def remove_hook(self, event=None, name=None, ref=None):
        if ref != None:
            for i in range(0, len(self.mp_hooks)):
                if self.mp_hooks[i] == ref:
                    self.mp_hooks.pop(i)
                    log.debug('hook found and removed')
                    break
            return
        
        log.debug('no hook found')

