#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from main import APP
from music_pi.mp_timer import MP_Timer
from control.interface import Hw_Interface as hwi
from control.display import Display

from logger import log
from time import sleep, strftime

class Work_Flow:
    

    MP_POWER_ON = 0

    MP_STANDBY  = 8
    MP_ON       = 16
    MP_MENU     = 32
    
    MP_WAKEUP   = 2 # stand-by to on
    MP_GOSLEEP  = 4 # on to stand-by

    MP_GOON     = 15 # menu to on
    MP_GOMENU   = 31 # on to menu
    
    #MP_EXITMENU = 33 
    
    
    
    def __init__(self):
        self.go_poweron()
        
        self.switcher = { # tryby pracy urządzenia
            self.MP_POWER_ON:   self._lev_power_on,  
                  
            self.MP_STANDBY:    self._lev_standby,
            self.MP_ON:         self._lev_on,
            self.MP_MENU:       self._lev_menu,
            
            self.MP_WAKEUP:     self._lev_go_wakeup,
            self.MP_GOSLEEP:    self._lev_go_sleep,

            self.MP_GOON:       self._lev_go_on,
            self.MP_GOMENU:     self._lev_go_menu,
        }
        
        self._scrool_timer = MP_Timer(1, self._software_loop)
        self._scrool_timer.start()
        
        pass
    
    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 
    # -=-=- Tryby pracy -=-=- 
    
    '''
    Definicje trybów pracy
    
    |      | >  GO_ON  > |      | > GO_SLEEP > |          | > POWER_OFF > |        |
    | MENU |             |  ON  |              | STAND_BY |               | NO_APP |
    |      | < GO_MENU < |      | < WAKE_UP  < |          | < POWER_ON  < |        |
    
    NO_APP - kiedy aplikacja jest uruchamiana lub wyłączana z całym rasperry pi
        POWER_OFF - wyłączenie aplikacji razem z bezpiecznym wyłączeniem systemu
        POWER_ON  - uruchomienie aplikacji
    STAND_BY - ekran uśpienia, zwykle data i godzina
        GO_SLEEP - wejście w uśpienie (z pożegnaniem)
        WAKE_UP    - przywitanie i przejsćie w tryb uruchomienia
    ON - normalny tryb pracy wyświetlający czas utowru oraz nazwę
        GO_MENU- przejście do menu
        GO_ON - powrót do ekranu podstawowego
    MENU - przeszukiwanie menu za pomocą prostego interfejsu
    
    '''  
    
    def _software_loop(self):
        '''
        '''            
        # wybieranie za pomocą dictionary
        func = self.switcher.get(self.mp_status, lambda: None)
        
        # uruchomienie funkcji
        func()
        
    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 
    # -=-=- Zmiana tryby pracy -=-=-  
        
    # Wrapery przez sterowanie sprzętowe przycisków
    def go_poweron(self):
        log.info('go_poweron')
        self.mp_status = self.MP_POWER_ON
        #remap keys
    
    def go_wakeup(self):
        log.info('go_wakeup')
        self.mp_status = self.MP_WAKEUP
        
    def go_sleep(self):
        log.info('go_sleep')
        self.mp_status = self.MP_GOSLEEP
    
    def go_menu(self):
        log.info('go_menu')
        self.mp_status = self.MP_GOMENU
    
    def go_on(self):
        log.info('go_on')
        self.mp_status = self.MP_GOON      
    
        
    #---- tryby pracy
    def _lev_power_on(self):
        '''
        Zaraz po włączeniu zasilania i odpaleniu systemu
        Wstępna konfiguracja oraz przywitanie
        '''
        hwi.pwm_set(1, 1, 0, 0)
        hwi.lcd_clear()
        
        hwi.lcd_print(("{0} v{1}").format(APP['NAME'], APP['VER']), Display.ALIGN_RIGHT, 0, 0)
        hwi.lcd_print(("{0}").format(APP['AUTHOR']), Display.ALIGN_LEFT, 1, 0)
        hwi.lcd_print("Stacja uruchomiona", Display.ALIGN_CENTER, 3, 0)
        
        #sleep(1)
        
        self.mp_status = self.MP_STANDBY # przejście do uśpienia
        
        hwi.key_clear_map()
        hwi.key_map(hwi.KEY_PRW, self.go_wakeup, hwi.KEY_STATE_DOWN)

        # mapowanie przycisku wybudzenia
        #self.control.clear_map()
        #self.control.map_key(self.control.KEY_PRW, self.go_wakeup)
        #hwi.lcd_clear()
        
     
    def _exit(self):  
        '''
        Wyłączenie całkowite urządzenia
        @todo: wygaszenie urzadzenia fizyczne, i przygotowanie poprzez lcd
        ''' 
        
        hwi.lcd_clear()
        
        hwi.lcd_print("  .   '  .   bye  * ", Display.ALIGN_LEFT, 0, 0)
        hwi.lcd_print(".    ,    +   .   . ", Display.ALIGN_LEFT, 1, 0)
        hwi.lcd_print("  *  bye  *  .    . ", Display.ALIGN_LEFT, 2, 0)
        hwi.lcd_print(" .  *  *  .   +     ", Display.ALIGN_LEFT, 3, 0)

        
    def _lev_standby(self):
        '''
        Stan oczekiwania czyli urządzenie jest wyłączone ale nadal wyświetla zegar 
        i powiadomomienia
        '''  
                
        hwi.lcd_clear()
        hwi.lcd_print(strftime('%H:%M:%S'), Display.ALIGN_CENTER, 0)
        hwi.lcd_print(strftime('%Y-%m-%d'), Display.ALIGN_CENTER, 2)   
        
        
    def _lev_menu(self):
        '''
        menu opcji
        '''

        pass  
    
    def _lev_on(self):
        '''
        Normalna praca urządzenia
        '''
        #self.update_song_info()
        hwi.lcd_clear()
        hwi.lcd_print("ON", Display.ALIGN_LEFT, 0, 0)
        
    #---- tryby przejściowe     
         
    def _lev_go_wakeup(self):
        '''
        Informacja startowa + włączenie urządzenia
        '''

        hwi.pwm_set(20, 20, 1, 1)
        hwi.lcd_clear()
        hwi.lcd_print(("{0} v{1}").format(APP['NAME'], APP['VER']), Display.ALIGN_RIGHT, 0, 0)
        hwi.lcd_print(("{0}").format(APP['AUTHOR']), Display.ALIGN_LEFT, 1, 0)
        
        hwi.lcd_print("Witaj", Display.ALIGN_CENTER, 3, 0)

        #sleep(1)
        '''
        @todo: future 
        
        self.disp.clear()
        self.disp.print_display("Wznawianie", self.disp.ALIGN_LEFT, 0, 0)
        self.disp.print_display("Playlisty:", self.disp.ALIGN_LEFT, 1, 0)
        self.disp.print_display("0", self.disp.ALIGN_RIGHT, 1, 0)
        self.disp.print_display("Sieć:", self.disp.ALIGN_LEFT, 2, 0)
        self.disp.print_display("Tak", self.disp.ALIGN_RIGHT, 2, 0)
        self.disp.print_display("USB:", self.disp.ALIGN_LEFT, 3, 0)
        self.disp.print_display("Brak", self.disp.ALIGN_RIGHT, 3, 0)
          
        sleep(1)
        '''
        
        self.mp_status = self.MP_GOON # przejście do normalnej pracy
        #self.control.clear_map()        
        
    
    def _lev_go_sleep(self):
        '''
        Wyłączanie urządzenia
        '''
        
        #self.mpdc.playback_stop()
        hwi.lcd_clear()
        hwi.lcd_print("", Display.ALIGN_LEFT, 0, 0)
        hwi.lcd_print("Do usłyszenia", Display.ALIGN_CENTER, 1, 0)
        hwi.lcd_print("music Pi v0.1", Display.ALIGN_RIGHT, 2, 0)
        hwi.lcd_print("", Display.ALIGN_LEFT, 3, 0)
    
        #sleep(1)
                 
        #self.disp.pwm_set(self.LED_OFF)          
        self.mp_status = self.MP_STANDBY
        hwi.key_clear_map()
        hwi.key_map(hwi.KEY_PRW, self.go_wakeup, hwi.KEY_STATE_DOWN)
        hwi.lcd_clear()
        hwi.pwm_set(1,1,0,0)

    
    def _lev_go_menu(self):
        '''
        Uruchomione menu opcji
        '''
        self.disp.clear()
        self.disp.print_display("Menu", self.disp.ALIGN_LEFT, 0, 0)
        
        self.control.clear_map()
        self.control.map_key(self.control.KEY_MENU, None)
        self.control.map_key(self.control.KEY_BACK, self.go_on)
    
    def _lev_go_on(self):
        #self.control.map_key(self.control.KEY_PRW, self.go_sleep)
        #self.control.map_key(self.control.KEY_MENU, self.go_menu)
        #self.control.map_key(self.control.KEY_PLAY, self.mpdc.playback_play)
        #self.control.map_key(self.control.KEY_NEXT, self.mpdc.playback_next)
        #self.control.map_key(self.control.KEY_PREV, self.mpdc.playback_previous)
        hwi.lcd_clear()
        self.mp_status = self.MP_ON
        
        hwi.key_clear_map()
        hwi.key_map(hwi.KEY_PRW, self.go_sleep, hwi.KEY_STATE_DOWN)