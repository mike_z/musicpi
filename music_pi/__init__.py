#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''



from time import sleep, strftime
import threading

#from main import APP_NAME, APP_VER, APP_AUTHOR

import mpdc


from control.display import Display
from control.layer_record import Layer_Record
from control.keybrd import Keybrd
from control.pwm import PWM
from control.encoder import Encoder

from config import hw_lcd_conf, hw_pwm_cong, hw_key_conf

from control.key_map import Key_map
from music_pi.work_flow import Work_Flow
from sw_state import SW_State


from logger import log



'''
@todo: restart modułu jeśli się wysypie
'''



class music_pi:
    '''
    Music Pi main software
    '''
        
    def __init__(self):
        '''
        Inicjuje i uruchamia od razu całe urządzenie
        W rzeczywistości uruchamia inicjalizację sprzętu oraz oprogramowania
        '''
        log.debug("init music_pi")
        
        self.hw_dev = dict()
        self._init_hw()  # uruchomienie sprzętu
        self._init_sw()  # uruchomienie oprogramowania
        
        
        '''        
        # status główny urządzenia
        #self.mp_status = self.MP_POWER_ON
        self.go_poweron()
        self.song_disp = None
        '''
        
    def __del__(self):
        '''
        Sprzątanie obiektu
        Watki urządzeń oraz oprogramowania wyagają zatrzymania pętli
        obsługi danych urządzeń
        '''
        log.debug('del music_pi')
        # odwrotna kolejność sw -> hw (wysyłane są jeszcze sygnały)
        self._del_sw()
        self._del_hw()
        
        
    def _init_hw(self):
        '''
        Inicjalizacja sprzętu
        Uruchomienie urządzeń oraz sterowników z odpowiednią konfiguracją
        '''
        log.info('_init_hw')
        
        self.hw_dev['DISPLAY'] = Display(hw_lcd_conf)
        self.hw_dev['KEYBRD'] = Keybrd(hw_key_conf)
        self.hw_dev['PWM'] = PWM(hw_pwm_cong)
        self.hw_dev['ENC'] = Encoder(hw_key_conf)
        
                
    def _del_hw(self):
        '''
        Ręczne sprzątanie po sprzęcie
        '''
        log.debug('------ hardware stop ------')
        self._hw_loop_valid = False
        
        for dev in self.hw_dev:
            try:
                self.hw_dev[dev].__del__()
            except:
                log.fatal('Usunięcie urzadzenia {0} nie powiodło się'.format(dev))

    def _init_sw(self):
        '''
        Inicjalizacja oprogramowania
        '''
        log.info('_init_sw')

        
        self.keymap = Key_map() # Mapa przycisków
        
        #self.mpdc = mpdc.mpdclient()
        #self.mpdc.connect()             # klient MPC
        
        #self.wf = Work_Flow()    # Obsługa menu i api na wyświetlaczu
        self.state = SW_State()
     
    def _del_sw(self):
        log.debug('------ software stop ------')
        #self._sw_loop_valid = False
             
    
    def is_alive(self):
        '''
        Status dla wątku perwotnego z linii poleneń
        '''
        
        return self.state._thread_valid
        #if self.mp_status != self.MP_EXIT:
            #if not self._hw_loop_valid and not self._sw_loop_valid:
            #    return True
            #else:
            #    return False
        #return True
        #else:
        #    return False   
