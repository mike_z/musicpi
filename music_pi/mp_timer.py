#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

import threading
from logger import log
from music_pi.mp_events import MP_Events
from music_pi.event_map import Event_Map
from time import sleep

class MP_Timer:
    '''
    Timer dla uruchamiania zdażeń niezależnie
    
    Obiekt powstał na wzór windowsowego timera dla formatek WinApi 
    gdzie wydarzenie jest generowane porzez Timer uruchamiany 
    po przekroczeniu zliczania, uruchamia konkretną wskazaną funkcję.  
    '''
    
    TIMER_ID = 0 # globalny licznik timerów
    
    def __init__(self, time, event_target):
        '''
        Inicjacja timiera (nie uruchamia samego timera)
        
        Aby włączyć użyj start!
        
        Args:
            time: czas odstępu w sekudanch co ile ma się uruchamiać
            event_target: referencja do metody odpalanej przez wydażenie.
        '''
        self._time = time
        self._event_target = event_target
        self._id = MP_Timer.TIMER_ID + 0
        MP_Timer.TIMER_ID += 1

    def _loop(self):
        '''
        Pętla timera - tylko do użytku wewnętrz obiektu
        '''
        # Pętla timera 
        while self._loop_valid:
            sleep(self._time)
            self.call()
        
    def call(self):
        '''
        Wywołanie generowane przez kolejny cykl zegarowy timera
        '''
        ev = MP_Events.Instance()
        ev.new_event(self, Event_Map.EV_TIMER, {'timer':self._id})
        
    def callback(self, source, data):
        '''
        Wywołanie generowane przez Events na okoliczność flagi wywołania
        '''
        if source == self:
            try:
                self._event_target(source, data)
            except(TypeError):
                self._event_target()
            except:
                log.error('Timer-{0} callback error'.format(self._id))
                
    def start(self): 
        '''
        Uruchomienie timera
        Tworzy nowy wątek programu zajmujący się liczeniem czasu
        Podpina obsługę poprzez Events.hook
        '''   
        ev = MP_Events.Instance()
        ev.add_hook(Event_Map.EV_TIMER, 'Timer-{0}'.format(self._id), self.callback)
             
        self._loop_valid = True
        self._timer_thread = threading.Thread(target=self._loop, name='Timer-{0}'.format(self._id))
        self._timer_thread.daemon = True # zakończy się razem z głównym wątkiem
        self._timer_thread.start()
        log.debug('new MP_Timer id:{0}'.format(self._id))
        
    def stop(self):
        '''
        Zatrzymanie timera i usunięcie z listy hooks
        '''
        self._loop_valid = False
        ev = MP_Events.Instance()
        ev.remove_hook(ref=self._event_target)
    
        
        