#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from logger import log

# wiadomość systemowa
class MP_msg:
    def __init__(self, source, event, data, hook=None):
        self.source = source
        self.event = event
        self.data = data
        self.hook = hook
        #log.debug('new system message')
        
    def get(self):
        return {'source':self.source, 'event':self.event, 'data':self.data, 'hook':self.hook}

