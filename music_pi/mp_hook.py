#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from logger import log

# zaczepienie/uchwycenie wydażenia
class MP_hook:
    def __init__(self, event, name, ref):
        self.event = event
        self.name = name
        self.ref = ref
        
    def get(self):
        return {'event':self.event, 'name':self.name, 'ref':self.ref}
