#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''


class Event_Map:
    EV_POWER_ON = 0
    EV_POWER_OFF = 1
    
    EV_TIMER = 10
    EV_STATUS_CHANGE = 20
    
    # LCD
    EV_LCD_UPDATE = 100
    EV_LCD_UPTEXT = 101
    EV_LCD_CLEAR = 102
    EV_LCD_PRINT = 103
    EV_LCD_SCROLL_RST = 104
    
    # KEYS
    EV_KEY_DOWN = 111
    EV_KEY_UP = 112
    EV_KEY_MAP = 115
    EV_KEY_MCLEAR = 116
        
    # PWM
    EV_PWM_SET = 120
    
    # ENCODER
    EV_ENC = 130
    
    
    
    
    