#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

'''
Plik konfiguracji 
'''

hw_lcd_conf = dict()

hw_lcd_conf['LCD_NUM']       = 2             # Liczba wyświetlaczy
hw_lcd_conf['ROW_SIZE']      = 20            # Szerokość wiersza
hw_lcd_conf['ROW_NUM']       = 2             # Ilość wierszy dla jednego wyświetlacza
hw_lcd_conf['LCD_PCF']       = [0x38, 0x38]  # Adresy sterownika PCF8574
hw_lcd_conf['LCD_PIN_E']     = [16, 18]      # Linia Enable (selektor, zastrzask)
hw_lcd_conf['LCD_PIN_RS']    = [22, 22]      # Sygnał RS (wg. numeracji portu nie GPIO)
hw_lcd_conf['UPDATE_TIME']   = 0.10

hw_key_conf = dict()

hw_key_conf['KEY_PCF']      = 0x39  # adres PCF8574
hw_key_conf['KEY_INT']      = 15    # sygnał przerwania INT
hw_key_conf['ENC_A']        = 29    # sygnał A encodera
hw_key_conf['ENC_B']        = 31    # sygnał B encodera
hw_key_conf['UPDATE_TIME']  = 0.05
hw_key_conf['UPDATE_TIME2']  = 0.5

hw_pwm_cong = dict()

hw_pwm_cong['PWM_ADR']  = 0x62  # adres układu PCA9633
hw_pwm_cong['PWM_LCD0'] = 20    # sygnał sterowania backlight lcd0
hw_pwm_cong['PWM_LCD1'] = 20    # sygnał sterowania backlight lcd1
hw_pwm_cong['PWM_KEY']  = 20    # podświetlanie klawiszy
hw_pwm_cong['PWM_ENC']  = 20    # podświetlanie encodera