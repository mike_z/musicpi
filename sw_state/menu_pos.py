#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''


class MenuOpt:
    
    def __init__(self, tekstList='', goTo=None, getVal=None, setVal=None, subMenu=None):
    
        self.tekstList = tekstList  # napis
        self.goTo = goTo            # funkcja
        self.getVal = getVal        # pobierz wartość
        self.setVal = setVal        # zmien wartość
        self.subMenu = subMenu      # lista submenu jeśli nie None
        
    
    