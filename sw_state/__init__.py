#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''

from main import APP
from main import ST
from main import IS_ALIVE
from music_pi.mp_timer import MP_Timer
from control.interface import Hw_Interface as hwi
from control.display import Display
from mpdc import MpdClient as mpdc

from logger import log
from time import sleep, strftime
import threading

from sw_state.clock_state import ClockState
from sw_state.status_state import StatusState
from sw_state.menu_state import MenuState

from music_pi.event_map import Event_Map
from music_pi.mp_events import MP_Events
from music_pi.pios import PiOS


class SW_State:
    

    MP_POWER_ON = 0
    MP_SHUTDOWN = 1

    MP_STANDBY  = 8
    MP_ON       = 16
    MP_MENU     = 32
    
    MP_WAKEUP   = 2 # stand-by to on
    MP_GOSLEEP  = 4 # on to stand-by

    MP_GOON     = 15 # menu to on
    MP_GOMENU   = 31 # on to menu
    
    
    
    #MP_EXITMENU = 33 
    
    def __init__(self):
        self.mp_status = self.MP_POWER_ON
        
        ev = MP_Events.Instance()
        ev.add_hook(Event_Map.EV_STATUS_CHANGE, 'STATUS_CHANGE', self.go_to_callback)
        
        
        self.switcher = { # tryby pracy urządzenia
            self.MP_POWER_ON:   self._lev_power_on,  
                  
            self.MP_STANDBY:    self._lev_standby,
            self.MP_ON:         self._lev_on,
            self.MP_MENU:       self._lev_menu,
            
            self.MP_WAKEUP:     self._lev_go_wakeup,
            self.MP_GOSLEEP:    self._lev_go_sleep,

            self.MP_GOON:       self._lev_go_on,
            self.MP_GOMENU:     self._lev_go_menu,
            
            self.MP_SHUTDOWN:     self._exit,
        }
        
        self._thread_valid = True
        self._thread = threading.Thread(target=self._software_loop, name='SW_TH')
        self._thread.daemon = True # zakończy się razem z głównym wątkiem
        self._thread.start()
        
        
        ev.add_hook(Event_Map.EV_ENC, 'ENC_CHANGE', self.enc_callback)
        
        #mi = mpdc.Instance()
        #ST['VOL'] = int(mi.get_volume())
        
    
    
    
    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 
    # -=-=- Tryby pracy -=-=- 
    
    '''
    Definicje trybów pracy
    
    |      | >  GO_ON  > |      | > GO_SLEEP > |          | > POWER_OFF > |        |
    | MENU |             |  ON  |              | STAND_BY |               | NO_APP |
    |      | < GO_MENU < |      | < WAKE_UP  < |          | < POWER_ON  < |        |
    
    NO_APP - kiedy aplikacja jest uruchamiana lub wyłączana z całym rasperry pi
        POWER_OFF - wyłączenie aplikacji razem z bezpiecznym wyłączeniem systemu
        POWER_ON  - uruchomienie aplikacji
    STAND_BY - ekran uśpienia, zwykle data i godzina
        GO_SLEEP - wejście w uśpienie (z pożegnaniem)
        WAKE_UP    - przywitanie i przejsćie w tryb uruchomienia
    ON - normalny tryb pracy wyświetlający czas utowru oraz nazwę
        GO_MENU- przejście do menu
        GO_ON - powrót do ekranu podstawowego
    MENU - przeszukiwanie menu za pomocą prostego interfejsu
    
    '''  
    
    def _software_loop(self):
        '''
        ''' 
        while self._thread_valid:
                
            # wybieranie za pomocą dictionary
            func = self.switcher.get(self.mp_status, lambda: None)
            
            # uruchomienie funkcji
            func()
    
    def go_to_callback(self, source, data):
        log.warning("go #" + str(data))
        self.mp_status = data
        
    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 
    # -=-=- tryby pracy -=-=-  
        
    def _lev_power_on(self):
        '''
        Zaraz po włączeniu zasilania i odpaleniu systemu
        Wstępna konfiguracja oraz przywitanie
        '''

        hwi.pwm_set(ST['PWM_DISP_PLAY'], ST['PWM_DISP_PLAY'], ST['PWM_KEY_PLAY'], ST['PWM_KEY_PLAY'])
        hwi.lcd_clear()
        
        hwi.lcd_print(("{0} v{1}").format(APP['NAME'], APP['VER']), Display.ALIGN_RIGHT, 0, 0)
        hwi.lcd_print(("{0}").format(APP['AUTHOR']), Display.ALIGN_LEFT, 1, 0)
        hwi.lcd_print("Stacja uruchomiona", Display.ALIGN_CENTER, 3, 0)
        
        sleep(1)
        
        
        hwi.pwm_set(ST['PWM_DISP_STBY'],ST['PWM_DISP_STBY'],ST['PWM_KEY_STBY'],ST['PWM_KEY_STBY'])
        ClockState.goState()
        
        self.mp_status = self.MP_STANDBY # przejście do uśpienia
       
     
    def _exit(self):  
        '''
        Wyłączenie całkowite urządzenia
        @todo: wygaszenie urzadzenia fizyczne, i przygotowanie poprzez lcd
        ''' 
        
        hwi.lcd_clear()
        
        hwi.lcd_print("  .   '  .   bye  * ", Display.ALIGN_LEFT, 0, 0)
        hwi.lcd_print(".    ,    +   .   . ", Display.ALIGN_LEFT, 1, 0)
        hwi.lcd_print("  *  bye  *  .    . ", Display.ALIGN_LEFT, 2, 0)
        hwi.lcd_print(" .  *  *  .   +     ", Display.ALIGN_LEFT, 3, 0)
        
        self._thread_valid = False
        
        PiOS.shutdown()
        
        IS_ALIVE = False #@UnusedVariable
        
        sleep(3)

        
    def _lev_standby(self):
        '''
        Stan oczekiwania czyli urządzenie jest wyłączone ale nadal wyświetla zegar 
        i powiadomomienia
        '''  
        ClockState.useState() 
        
        
        
    def _lev_menu(self):
        '''
        menu opcji
        '''
        ms = MenuState.Instance()
        ms.useState()
        sleep(1)

        pass  
    
    def _lev_on(self):
        '''
        Normalna praca urządzenia
        '''
        ss = StatusState.Instance()
        ss.useState()
        sleep(1)
        
    #---- tryby przejściowe     
         
    def _lev_go_wakeup(self):
        '''
        Informacja startowa + włączenie urządzenia
        '''

        hwi.pwm_set(ST['PWM_DISP_PLAY'], ST['PWM_DISP_PLAY'], ST['PWM_KEY_PLAY'], ST['PWM_KEY_PLAY'])
        hwi.lcd_clear()
        hwi.lcd_print(("{0} v{1}").format(APP['NAME'], APP['VER']), Display.ALIGN_RIGHT, 0, 0)
        hwi.lcd_print(("{0}").format(APP['AUTHOR']), Display.ALIGN_LEFT, 1, 0)
        
        hwi.lcd_print("Witaj", Display.ALIGN_CENTER, 3, 0)

        sleep(1)     
        
        hwi.lcd_clear()
        hwi.lcd_print("Wznawianie", hwi.ALIGN_LEFT, 0, 0)
        hwi.lcd_print("Playlisty:", hwi.ALIGN_LEFT, 1, 0)
        hwi.lcd_print(str(mpdc.Instance().count_playlists()), hwi.ALIGN_RIGHT, 1, 0)
        hwi.lcd_print("Sieć:", hwi.ALIGN_LEFT, 2, 0)
        
        if PiOS.getNetworkState():
            hwi.lcd_print("Tak", hwi.ALIGN_RIGHT, 2, 0)
        else:
            hwi.lcd_print("Nie", hwi.ALIGN_RIGHT, 2, 0)
        
        sleep(1)
        
        self.mp_status = self.MP_GOON # przejście do normalnej pracy
        
    
    def _lev_go_sleep(self):
        '''
        Wyłączanie urządzenia
        '''
        
        self.mp_status = self.MP_STANDBY
        
        #self.mpdc.playback_stop()
        hwi.lcd_clear()
        hwi.lcd_print("", Display.ALIGN_LEFT, 0, 0)
        hwi.lcd_print("Do usłyszenia", Display.ALIGN_CENTER, 1, 0)
        hwi.lcd_print("music Pi v0.1", Display.ALIGN_RIGHT, 2, 0)
        hwi.lcd_print("", Display.ALIGN_LEFT, 3, 0)
    
        sleep(1)
        
        mi = mpdc.Instance()
        mi.playback_pause(1)
        #StatusState._go_pause()
        
        hwi.pwm_set(ST['PWM_DISP_STBY'],ST['PWM_DISP_STBY'],ST['PWM_KEY_STBY'],ST['PWM_KEY_STBY'])
        
        ClockState.goState()
        
        #sleep(1)

    
    def _lev_go_menu(self):
        '''
        Uruchomione menu opcji
        '''
        self.mp_status = self.MP_MENU
        ms = MenuState.Instance()
        ms.goState()

        sleep(1)
    
    def _lev_go_on(self):
        '''
        Odtwarzanie lub ekran statusu
        '''
        ##ClockState.leftState()
        self.mp_status = self.MP_ON
        ss = StatusState.Instance()
        ss.goState()
        #sleep(1)
        
        
    def enc_callback(self, source, data):
        mi = mpdc.Instance()
        
        log.warning('set to ' + str(data['val']) + " " + str(ST['VOL']))

        ST['VOL'] = mi.set_volume_rel(int(data['val']), ST['VOL'])
        log.warning('set to ' + str(data['val']))

        

        