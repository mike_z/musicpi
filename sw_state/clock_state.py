#!/usr/bin/python
# -*- coding: utf-8 -*-


from time import strftime, sleep
from control.display import Display
from control.interface import Hw_Interface as hwi
from logger import log
#import urllib



class ClockState:
    
    layer1 = None
    layer2 = None    
        
    def __init__(self):
        pass
    
    @staticmethod
    def goState():
        
        log.warning("Bindowanie wake up")
        
        hwi.key_clear_map()
        hwi.key_map(hwi.KEY_PRW, hwi.go_wakeup, hwi.KEY_STATE_DOWN)
        
        hwi.lcd_clear()

        
        
        if (ClockState.layer1 == None or ClockState.layer2 == None):
            ClockState.layer1 = hwi.lcd_print(strftime('%H:%M:%S'), hwi.ALIGN_CENTER, 0)
            ClockState.layer2 = hwi.lcd_print(strftime('%Y-%m-%d'), hwi.ALIGN_CENTER, 2) 
        else:
            ClockState.layer1.text = strftime('%H:%M:%S')
            ClockState.layer2.text = strftime('%Y-%m-%d')
            hwi.lcd_print_layer(ClockState.layer1)
            hwi.lcd_print_layer(ClockState.layer2)
            
        
    
    @staticmethod
    def leftState():
        ClockState.layer1 = None
        ClockState.layer2 = None 

    
    @staticmethod
    def useState():        
        ClockState.layer1.text = strftime('%H:%M:%S')
        ClockState.layer2.text = strftime('%Y-%m-%d')
        sleep(0.4) 

