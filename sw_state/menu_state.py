#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Odtwarzacz muzyczny w oparciu o platformę Raspberry Pi
Praca dyplomowa Michał Żak @ SWSIM 2016

@author: Michał Żak 
@copyright: CC BY-SA http://creativecommons.org/licenses/by-sa/4.0/
'''


from time import sleep

from control.interface import Hw_Interface as hwi
#import urllib
from logger import log
from mpdc import MpdClient as mpdc
from sw_state.menu_pos import MenuOpt

from music_pi.pios import PiOS
from main import APP
from main import ST

class MenuState:
    
    _instance = None
    
    scrLineT = []
    scrLineO = []
    scrLineM = []
    
    lineText = []     # Linie wpisów menu
    lineOpt = []      # symbol ustawienia po prawej
    lineMark = []     # symbol markera po lewej
    
    ROW_NUM = 4
    
    #---
    markPos = 0 # pozycja kursora
    markPosOld = 0
    needUpdate = True # flaga kontrolująca odświeżanie ekranu
    needScrool = True # flaga odświeżenia pozycji kursora
    menuPos = []
    menuTree = list()
    
    deviceMenu = None
    
    
    
    def __init__(self):
        log.debug('Nowy MenuState')
        
        for i in range(0,self.ROW_NUM):
            self.scrLineM.append(hwi.lcd_get_layer('', hwi.ALIGN_LEFT, i))
            self.scrLineT.append(hwi.lcd_get_layer('', hwi.ALIGN_CENTER, i))
            self.scrLineO.append(hwi.lcd_get_layer('', hwi.ALIGN_RIGHT, i))     
            
        # root menu
        menuTree = []
        
        # |-----------------
        # | menu update
        self.deviceMenu = MenuOpt('Aktualizacja', None, None, None, [])
        
        # |--| select dev 
        m_up = MenuOpt('Akt. kontynuować?', self.menu_updateLists, None, None, None)
        self.deviceMenu.subMenu.append(m_up)
        
        #...        
        menuTree.append(self.deviceMenu)
        
        # |-----------------
        # | menu ustawienia
        m_set = MenuOpt('Ustawienia', None, None, None, [])
        
        # |--| jasność ekran
        m_set_jep = MenuOpt('Jasn. ekranu', None, self.getJasnoscEkranuPlay, self.setJasnoscEkranuPlay, None)
        m_set.subMenu.append(m_set_jep)
        
        # |--| jasność klawisze
        m_set_jpp = MenuOpt('Jasn. przycis.', None, self.getJasnoscPrzyciskowPlay, self.setJasnoscPrzyciskowPlay, None)
        m_set.subMenu.append(m_set_jpp)
        
        # |--| stand-by jasność ekran
        m_set_jes = MenuOpt('St.By ekranu', None, self.getJasnoscEkranuStb, self.setJasnoscEkranuStb, None)
        m_set.subMenu.append(m_set_jes)
        
        # |--| stand-by jasność klawisze
        m_set_jps = MenuOpt('St.Być przycis.', None, self.getJasnoscPrzyciskowStb, self.setJasnoscPrzyciskowStb, None)
        m_set.subMenu.append(m_set_jps)
        
        # |--| auto play
        m_set_ap = MenuOpt('Auto. start', None, self.getAutoStart, self.setAutoStart, None)
        m_set.subMenu.append(m_set_ap)
        
        menuTree.append(m_set)
        
        # |-----------------
        # | menu about
        m_about = MenuOpt('O MusicPi', None, None, None, [])
        
        # |--| menu about
        m_about_tx1 = MenuOpt(("{0} v{1}").format(APP['NAME'], APP['VER']))
        m_about.subMenu.append(m_about_tx1)
        m_about_tx2 = MenuOpt(("{0}").format(APP['AUTHOR']))
        m_about.subMenu.append(m_about_tx2)
        m_about_tx3 = MenuOpt( 'Praca dyplomowa')
        m_about.subMenu.append(m_about_tx3)
        m_about_tx4 = MenuOpt('SWSIM')
        m_about.subMenu.append(m_about_tx4)
        menuTree.append(m_about)
        
        # |-----------------
        # | menu shutdown
        m_shutdown = MenuOpt('Wyłącz OS', None, None, None, [])
        
        # |--| confirm shutdown
        m_shutdown_con = MenuOpt('Czy na pewno', self.shutdown_pi, None, None, None)
        m_shutdown.subMenu.append(m_shutdown_con)
        m_shutdown_con2 = MenuOpt('wyłączyć OS ?', self.shutdown_pi, None, None, None)
        m_shutdown.subMenu.append(m_shutdown_con2)
        
        menuTree.append(m_shutdown)    
        
        self.menuTree = menuTree
           
        
    @staticmethod
    def Instance():
        if MenuState._instance == None:
            MenuState._instance = MenuState()
        
        return MenuState._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)
    
    def __del__(self):
        pass
        
    
    @staticmethod
    def goState():
        
        self = MenuState.Instance()
        
        hwi.key_clear_map()
        hwi.key_map(hwi.KEY_BACK, hwi.go_on, hwi.KEY_STATE_DOWN)
        hwi.key_map(hwi.KEY_NEXT, self.menuDown, hwi.KEY_STATE_DOWN)
        hwi.key_map(hwi.KEY_NFOL, self.menuIncr, hwi.KEY_STATE_DOWN)
        hwi.key_map(hwi.KEY_PREV, self.menuUp, hwi.KEY_STATE_DOWN)
        hwi.key_map(hwi.KEY_PFOL, self.menuDecr, hwi.KEY_STATE_DOWN)
        
        hwi.key_map(hwi.KEY_MENU, self.menuEnter, hwi.KEY_STATE_DOWN)
        hwi.key_map(hwi.KEY_BACK, self.menuBack, hwi.KEY_STATE_DOWN)
        
        hwi.lcd_clear()
        
        for i in range(0,self.ROW_NUM):
            self.scrLineM[i].text = ''
            hwi.lcd_print_layer(self.scrLineM[i])
            
            self.scrLineO[i].text = ''
            hwi.lcd_print_layer(self.scrLineO[i])
            
            self.scrLineT[i].text = ''
            hwi.lcd_print_layer(self.scrLineT[i])
                        
        self.needUpdate = True
        self.needScrool = True
        self.menuPos = []
        self.markPos = 0
        self.markPosOld = 0
    
    @staticmethod
    def leftState():
        pass

    
    @staticmethod
    def useState():
        self = MenuState.Instance()
        
        # print menu option
        if self.needUpdate is True:
            self.menuUpdate()
            self.needUpdate = False
            self.needScrool = True

        # scroll zawartości gdy liczba opcji większa niż na ekranie
        if self.needScrool is True:
            self.menuScrool()
            self.needScrool = False
        
        sleep(0.2)
        

    #--------------------------------------      
    
    def menuUp(self):
                
        if self.markPos > 0:
            self.markPos -= 1

        log.debug("menuUp")
        self.needUpdate = True
    
    def menuDown(self):

        curMenuOpt = self.getCurrentMenuTree()
        
        if self.markPos < len(curMenuOpt)-1:
            self.markPos += 1

        log.debug("menuDown")
        self.needUpdate = True
    
    def menuEnter(self):
        
        log.warning('menuEnter')
        
        curMenuOpt = self.getCurrentMenuTree()
        
        if curMenuOpt[self.markPos].goTo is not None:
            curMenuOpt[self.markPos].goTo()
            
        if curMenuOpt[self.markPos].subMenu is not None:
            
            self.menuPos.append(self.markPos+0)
            self.markPos = 0
            self.markPosOld = 0
            self.needUpdate = True # flaga kontrolująca odświeżanie ekranu
            self.needScrool = True # flaga odświeżenia pozycji kursora
        
    
    def menuBack(self):
        
        #curMenuOpt = self.getCurrentMenuTree()
        
        if len(self.menuPos) == 0:
            hwi.go_on()
        if len(self.menuPos) > 0:
            self.markPos = self.menuPos.pop()
            self.markPosOld = self.markPos + 0
            self.needUpdate = True # flaga kontrolująca odświeżanie ekranu
            self.needScrool = True # flaga odświeżenia pozycji kursora
     
    def menuIncr(self):
        
        curMenuOpt = self.getCurrentMenuTree()
        
        if curMenuOpt[self.markPos].setVal is not None:
            curMenuOpt[self.markPos].setVal(1)
        
        #if curMenuOpt[self.markPos].goTo is not None:
        #    curMenuOpt[self.markPos].goTo()
    
    def menuDecr(self):
        
        curMenuOpt = self.getCurrentMenuTree()
        
        if curMenuOpt[self.markPos].setVal is not None:
            curMenuOpt[self.markPos].setVal(-1)
            
        #if curMenuOpt[self.markPos].goTo is not None:
        #    curMenuOpt[self.markPos].goTo()
        
        pass

    #--------------------------------------
    
    def getCurrentMenuTree(self):
        
        curMenuOpt = None
        
        if len(self.menuPos) == 0:
            curMenuOpt = self.menuTree
        elif len(self.menuPos) == 1:
            curMenuOpt = self.menuTree[self.menuPos[0]].subMenu
        elif len(self.menuPos) == 2:
            curMenuOpt = self.menuTree[self.menuPos[0]].subMenu[self.menuPos[1]].subMenu
        elif len(self.menuPos) == 3:
            curMenuOpt = self.menuTree[self.menuPos[0]].subMenu[self.menuPos[1]].subMenu[self.menuPos[2]].subMenu
        else:
            curMenuOpt = self.menuTree
            
        return curMenuOpt
    
    def menuUpdate(self): 
        
        log.debug('menuUpdate ENTER') 
        
        curMenuOpt = self.getCurrentMenuTree()
        
        self.lineText = [] # Linie wpisów menu
        #self.lineOpt  = [] # symbol ustawienia po prawej
        #self.lineMark = [] # symbol markera po lewej
                
        for i in range(0,len(curMenuOpt)):
            log.debug('menuUpdate ' + curMenuOpt[i].tekstList) 
            self.lineText.append(curMenuOpt[i].tekstList)
             
    def menuScrool(self):
        
        if self.needUpdate:
            self.menuUpdate()
        
        curMenuOpt = self.getCurrentMenuTree()
        self.lineOpt  = [] # symbol ustawienia po prawej
        self.lineMark = [] # symbol markera po lewej
        
        for i in range(0,len(curMenuOpt)):
            
            if curMenuOpt[i].getVal is not None:
                self.lineOpt.append(curMenuOpt[i].getVal())
            else:
                self.lineOpt.append('')
                
            if self.markPos == i:
                self.lineMark.append(chr(hwi.SYMBOL_ARROW_RIGHT))
            else:
                self.lineMark.append('')                        # symbol markera po lewej
        
        self.updateScreen()
        
    def updateScreen(self):
        
        curMenuOpt = self.getCurrentMenuTree()
        offset = 0
        
        if len(curMenuOpt) <= self.ROW_NUM:
            offset = 0 
        
        elif self.markPos in range(0,3):
            offset = 0 
        
        elif self.markPos == len(curMenuOpt)-1:
            offset = len(curMenuOpt)-self.ROW_NUM
        
        else:
            offset = self.markPos-3
            
        
        
        '''
        if self.markPosOld - self.markPos > 0:
            offset = self.markPos - 1
        elif self.markPosOld - self.markPos < 0:
            offset = self.markPos + 1
          
        if len(curMenuOpt) <= self.ROW_NUM:
            offset = 0         
        elif self.markPos < 2:
            offset = 0
        elif self.markPos > len(curMenuOpt) - 1 - self.ROW_NUM:
            offset = len(curMenuOpt) - 1 - self.ROW_NUM
        '''
        
        log.warn('scrLineM ' + str(len(self.scrLineM))) 
        log.warn('lineMark ' + str(len(self.lineMark)))         
        
        for i in range(0, self.ROW_NUM): 
            log.warn('i ' + str(i)) 
            if offset+i < len(self.lineMark):
                self.scrLineM[i].text = self.lineMark[offset+i]
                self.scrLineT[i].text = self.lineText[offset+i]
                self.scrLineO[i].text = self.lineOpt[offset+i]   
            else:
                self.scrLineM[i].text = ''
                self.scrLineT[i].text = ''
                self.scrLineO[i].text = ''   
            

    #--------------------------------------------
                 
    @staticmethod
    def menu_lists():
        mi = mpdc.Instance()
        
        MenuState.pushToScreen(['Aktualizacja'],0,0)
        sleep(2)
        
        mi.update_usb_lists()
        listy = mi.get_playlists()
        toScreen = []
        for lista in listy:
            toScreen.append(lista['playlist'] + " " + str(len(mi._client.listplaylistinfo(lista['playlist']))))
            
        MenuState.markMax = len(toScreen)-1    
        MenuState.pushToScreen(toScreen, MenuState.markPos, MenuState.markMax)
        
    #--------------------------------------------
    
    def menu_updateLists(self):
                
        mi = mpdc.Instance()
        
        self.deviceMenu.subMenu = [] # czyszczenie gałęzi
        m_up = MenuOpt('Aktualizacja', None, None, None, None)
        self.deviceMenu.subMenu.append(m_up) #dodanie napisu
        self.needUpdate = True
        
        sleep(2)
        
        # pobranie listy z urzadzeniami
        mi.update_usb_lists()
        listy = mi.get_playlists()
        toScreen = list()
        for lista in listy:
            toScreen.append(lista['playlist'] + " " + str(len(mi._client.listplaylistinfo(lista['playlist']))))
        
        # przepisanie listy do menu
        self.deviceMenu.subMenu = []
        for i in range(0, len(toScreen)):
            dev_rec = MenuOpt(toScreen[i], self.menu_useplaylist, None, None, None)
            self.deviceMenu.subMenu.append(dev_rec)
        self.needUpdate = True

    def menu_useplaylist(self):
        mi = mpdc.Instance()
        
        # załadowanie listy po ID
        mi.use_playlist(self.markPos)
        
        self.deviceMenu.subMenu = []
        m_up = MenuOpt('Załadowano', self.menu_updateLists, None, None, None)
        self.deviceMenu.subMenu.append(m_up)
        
        self.needUpdate = True
        
        hwi.go_on()
    
    def getJasnoscEkranuPlay(self):
        return str(ST['PWM_DISP_PLAY'])
    
    def setJasnoscEkranuPlay(self, val=0):
        if val > 0 and ST['PWM_DISP_PLAY'] < 25:
            ST['PWM_DISP_PLAY'] += 1
        elif val < 0 and ST['PWM_DISP_PLAY'] > 0:
            ST['PWM_DISP_PLAY'] -= 1 
            
        hwi.pwm_set(ST['PWM_DISP_PLAY'],ST['PWM_DISP_PLAY'],ST['PWM_KEY_PLAY'],ST['PWM_KEY_PLAY'])    
        
        self.needScrool = True

    def getJasnoscPrzyciskowPlay(self):
        return str(ST['PWM_KEY_PLAY'])
    
    def setJasnoscPrzyciskowPlay(self, val=0):
        if val > 0 and ST['PWM_KEY_PLAY'] < 25:
            ST['PWM_KEY_PLAY'] += 1
        elif val < 0 and ST['PWM_KEY_PLAY'] > 0:
            ST['PWM_KEY_PLAY'] -= 1 
            
        hwi.pwm_set(ST['PWM_DISP_PLAY'],ST['PWM_DISP_PLAY'],ST['PWM_KEY_PLAY'],ST['PWM_KEY_PLAY'])    
        
        self.needScrool = True
    
    def getJasnoscEkranuStb(self):
        return str(ST['PWM_DISP_STBY'])
        pass
    
    def setJasnoscEkranuStb(self, val=0):
        if val > 0 and ST['PWM_DISP_STBY'] < 25:
            ST['PWM_DISP_STBY'] += 1
        elif val < 0 and ST['PWM_DISP_STBY'] > 0:
            ST['PWM_DISP_STBY'] -= 1  
        
        self.needScrool = True
    
    def getJasnoscPrzyciskowStb(self):
        return str(ST['PWM_KEY_STBY'])
        pass
    
    def setJasnoscPrzyciskowStb(self, val=0):
        if val > 0 and ST['PWM_KEY_STBY'] < 25:
            ST['PWM_KEY_STBY'] += 1
        elif val < 0 and ST['PWM_KEY_STBY'] > 0:
            ST['PWM_KEY_STBY'] -= 1    
        
        self.needScrool = True
    
    def getAutoStart(self):
        if ST['AUTOSTART']:
            return 'Tak'
        else:
            return 'Nie'
    
    def setAutoStart(self, val=0):
        if ST['AUTOSTART']:
            ST['AUTOSTART'] = False
        else:
            ST['AUTOSTART'] = True
        
        self.needScrool = True
    
    def shutdown_pi(self):
        
        hwi.go_shutdown()
        
        
        
        