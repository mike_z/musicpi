#!/usr/bin/python
# -*- coding: utf-8 -*-

#from mpd import MPDClient, MPDError, CommandError 
#import time 
#import datetime 
#from time import sleep
from control.interface import Hw_Interface as hwi
from mpdc import MpdClient as mpdc
#from mpdc.mpd_utils import MPDUtils
#import urllib
from main import ST


class StatusState:
    
    _instance = None
    
    status_layer = None
    time_layer = None
    text_layer = None
    vol_layer = None
    
    pl_layer = None
    
    cur_play = False
    
    PLAY_PLAY = 1
    PLAY_PAUSE = 2
    PLAY_STOP = 3
    
    
    def __init__(self):
        

        self.status_layer = hwi.lcd_get_layer("", hwi.ALIGN_LEFT, 0, 0)
        self.vol_layer = hwi.lcd_get_layer("", hwi.ALIGN_CENTER, 0, 0)
        self.pl_layer = hwi.lcd_get_layer("", hwi.ALIGN_RIGHT, 0, 0)
        
        self.time_layer = hwi.lcd_get_layer("", hwi.ALIGN_RIGHT, 1, 0)
        self.text_layer = hwi.lcd_get_layer("", hwi.SCROLL_TEXT, 2, 1)


    
    @staticmethod
    def Instance():
        if StatusState._instance == None:
            StatusState._instance = StatusState()
        
        return StatusState._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)
    
    def __del__(self):
        pass
    
    def goState(self):     
        
        hwi.key_clear_map()
        hwi.key_map(hwi.KEY_PRW, hwi.go_sleep, hwi.KEY_STATE_DOWN)
        hwi.key_map(hwi.KEY_MENU, hwi.go_menu, hwi.KEY_STATE_DOWN)
        hwi.key_map(hwi.KEY_NEXT, self._go_next, hwi.KEY_STATE_DOWN)
        hwi.key_map(hwi.KEY_PREV, self._go_prev, hwi.KEY_STATE_DOWN)
        
        if ST['AUTOSTART'] == True or mpdc.Instance().get_status()['state'] == 'play':
            self._go_play()
        else:
            self._go_pause()
        '''
        
        if mpdc.Instance().get_status()['state'] == 'play': #tylko poprawienie bindów klawiszy
            StatusState._go_play()
        else:
            StatusState._go_pause()
        '''      
        hwi.lcd_clear()
        

        #self.status_layer.text = ""
        #self.pl_layer.text = ""
        #self.pl_layer.text = ""
        #self.time_layer.text = ""
        #self.text_layer.text = ""
        hwi.lcd_print_layer(self.status_layer)
        hwi.lcd_print_layer(self.vol_layer)
        hwi.lcd_print_layer(self.pl_layer) 
        hwi.lcd_print_layer(self.time_layer)
        hwi.lcd_print_layer(self.text_layer) 
                
    def _go_play(self):
        mpdc.Instance().playback_play()
        self.cur_play = True
        hwi.lcd_rescroll()
        hwi.key_map(hwi.KEY_PLAY, self._go_pause, hwi.KEY_STATE_DOWN)
    
    def _go_pause(self):
        mpdc.Instance().playback_pause()
        self.cur_play = False
        hwi.lcd_rescroll()
        hwi.key_map(hwi.KEY_PLAY, self._go_play, hwi.KEY_STATE_DOWN)  
        
    def _go_next(self):
        mpdc.Instance().playback_next()
        hwi.lcd_rescroll()
    
    def _go_prev(self):
        mpdc.Instance().playback_previous()
        hwi.lcd_rescroll()
        

    def leftStatus(self):
                 
        pass
        
    def useState(self):
        mi = mpdc.Instance()
        
        if len(mi.get_current_playlist()) is 0:
            if mi.count_playlists() is 0:
                song_text = "Podłącz urządzenie"
            else:
                song_text = "Wybierz playlistę"
                '''
                @TODO Go to menu 
                ''' 
                
            self.status_layer.text = song_text
            self.time_layer.text = ''
            self.text_layer.text = ''
        else:   
            status_data = mi.get_status()
            song_data = mi.get_current_song()
            
            artist  = song_data.get('artist', None)
            title   = song_data.get('title', None)
            filename= song_data.get('file', None)
            bitrate = song_data.get('bitrate', None)
            volume  = status_data.get('volume', '0')

            song_text = ''
            
            if(artist is not None):
                song_text += chr(hwi.SYMBOL_FULL_BOX) + 'A:'
                song_text += artist + ' ' #+ chr(hwi.SYMBOL_LF)
            if(title is not None):    
                song_text += chr(hwi.SYMBOL_FULL_BOX) + 'T:'
                song_text += title + ' ' #+ chr(hwi.SYMBOL_LF)
            if(filename is not None):  
                song_text += chr(hwi.SYMBOL_FULL_BOX) + 'F:'
                song_text += filename + ' ' #+ chr(hwi.SYMBOL_LF)
            if(bitrate is not None): 
                song_text += chr(hwi.SYMBOL_FULL_BOX) + 'i:'
                song_text += bitrate + 'kbps'
            
            try:
                cursong = str(int(status_data.get('song')) + 1)
            except:
                cursong = "-"
                
            
            self.pl_layer.text = cursong + "/" + status_data.get('playlistlength', '-')
            self.status_layer.text = status_data.get('state', '')
            self.time_layer.text = mi.get_timestring()
            self.text_layer.text = song_text
            self.vol_layer.text = "v:{0}%".format(volume)
            
